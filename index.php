<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml" lang="en-US" xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
		

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

	<title>
	ADLC 	</title>

	
	<!--[if lte IE 8]>
	<script type="text/javascript" src="https://adlc.uad.ac.id/wp-content/themes/ADLC/js/html5shiv.js"></script>
	<![endif]-->

	
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	
		<link rel="shortcut icon" href="http://adlc.uad.ac.id/wp-content/uploads/2015/09/favicon.png" type="image/x-icon" />
	
	
	
	
	
	<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="ADLC &raquo; Feed" href="https://adlc.uad.ac.id/feed/" />
<link rel="alternate" type="application/rss+xml" title="ADLC &raquo; Comments Feed" href="https://adlc.uad.ac.id/comments/feed/" />
<meta property="og:title" content="Home"/><meta property="og:type" content="article"/><meta property="og:url" content="https://adlc.uad.ac.id/"/><meta property="og:site_name" content="ADLC"/><meta property="og:description" content=""/><meta property="og:image" content="http://adlc.uad.ac.id/wp-content/uploads/logo-adlc-new.png"/>		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/adlc.uad.ac.id\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.3.2"}};
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
	<link rel='stylesheet' id='wp-block-library-css'  href='https://adlc.uad.ac.id/wp-includes/css/dist/block-library/style.min.css?ver=5.3.2' type='text/css' media='all' />
<link rel='stylesheet' id='rs-plugin-settings-css'  href='https://adlc.uad.ac.id/wp-content/plugins/revslider/public/assets/css/settings.css?ver=5.0.8.5' type='text/css' media='all' />
<style id='rs-plugin-settings-inline-css' type='text/css'>
.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}
</style>
<link rel='stylesheet' id='avada-stylesheet-css'  href='https://adlc.uad.ac.id/wp-content/themes/ADLC/style.css?ver=3.7.3' type='text/css' media='all' />
<!--[if lte IE 9]>
<link rel='stylesheet' id='avada-shortcodes-css'  href='https://adlc.uad.ac.id/wp-content/themes/ADLC/shortcodes.css?ver=3.7.3' type='text/css' media='all' />
<![endif]-->
<link rel='stylesheet' id='fontawesome-css'  href='https://adlc.uad.ac.id/wp-content/themes/ADLC/fonts/fontawesome/font-awesome.css?ver=3.7.3' type='text/css' media='all' />
<!--[if lte IE 9]>
<link rel='stylesheet' id='avada-IE-fontawesome-css'  href='https://adlc.uad.ac.id/wp-content/themes/ADLC/fonts/fontawesome/font-awesome.css?ver=3.7.3' type='text/css' media='all' />
<![endif]-->
<link rel='stylesheet' id='avada-animations-css'  href='https://adlc.uad.ac.id/wp-content/themes/ADLC/css/animations.css?ver=3.7.3' type='text/css' media='all' />
<!--[if lte IE 8]>
<link rel='stylesheet' id='avada-IE8-css'  href='https://adlc.uad.ac.id/wp-content/themes/ADLC/css/ie8.css?ver=3.7.3' type='text/css' media='all' />
<![endif]-->
<!--[if IE]>
<link rel='stylesheet' id='avada-IE-css'  href='https://adlc.uad.ac.id/wp-content/themes/ADLC/css/ie.css?ver=3.7.3' type='text/css' media='all' />
<![endif]-->
<link rel='stylesheet' id='avada-media-css'  href='https://adlc.uad.ac.id/wp-content/themes/ADLC/css/media.css?ver=3.7.3' type='text/css' media='all' />
<link rel='stylesheet' id='avada-ipad-css'  href='https://adlc.uad.ac.id/wp-content/themes/ADLC/css/ipad.css?ver=3.7.3' type='text/css' media='all' />
<link rel='stylesheet' id='ms-main-css'  href='https://adlc.uad.ac.id/wp-content/plugins/masterslider/public/assets/css/masterslider.main.css?ver=3.0.6' type='text/css' media='all' />
<link rel='stylesheet' id='ms-custom-css'  href='https://adlc.uad.ac.id/wp-content/uploads/masterslider/custom.css?ver=14.3' type='text/css' media='all' />
<script type='text/javascript' src='https://adlc.uad.ac.id/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp'></script>
<script type='text/javascript' src='https://adlc.uad.ac.id/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<script type='text/javascript' src='https://adlc.uad.ac.id/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js?ver=5.0.8.5'></script>
<script type='text/javascript' src='https://adlc.uad.ac.id/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js?ver=5.0.8.5'></script>
<link rel='https://api.w.org/' href='https://adlc.uad.ac.id/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://adlc.uad.ac.id/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://adlc.uad.ac.id/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 5.3.2" />
<link rel="canonical" href="https://adlc.uad.ac.id/" />
<link rel='shortlink' href='https://adlc.uad.ac.id/' />
<link rel="alternate" type="application/json+oembed" href="https://adlc.uad.ac.id/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fadlc.uad.ac.id%2F" />
<link rel="alternate" type="text/xml+oembed" href="https://adlc.uad.ac.id/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fadlc.uad.ac.id%2F&#038;format=xml" />
<script>var ms_grabbing_curosr='https://adlc.uad.ac.id/wp-content/plugins/masterslider/public/assets/css/common/grabbing.cur',ms_grab_curosr='https://adlc.uad.ac.id/wp-content/plugins/masterslider/public/assets/css/common/grab.cur';</script>
<meta name="generator" content="MasterSlider 3.0.6 - Responsive Touch Image Slider" />
<meta name="generator" content="Powered by Slider Revolution 5.0.8.5 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
<link rel="icon" href="https://adlc.uad.ac.id/wp-content/uploads/2016/01/cropped-logo-uad-universitas-ahmad-dahlan-yogyakarta-150-32x32.png" sizes="32x32" />
<link rel="icon" href="https://adlc.uad.ac.id/wp-content/uploads/2016/01/cropped-logo-uad-universitas-ahmad-dahlan-yogyakarta-150-192x192.png" sizes="192x192" />
<link rel="apple-touch-icon-precomposed" href="https://adlc.uad.ac.id/wp-content/uploads/2016/01/cropped-logo-uad-universitas-ahmad-dahlan-yogyakarta-150-180x180.png" />
<meta name="msapplication-TileImage" content="https://adlc.uad.ac.id/wp-content/uploads/2016/01/cropped-logo-uad-universitas-ahmad-dahlan-yogyakarta-150-270x270.png" />

	
	<!--[if lte IE 8]>
	<script type="text/javascript">
	jQuery(document).ready(function() {
	var imgs, i, w;
	var imgs = document.getElementsByTagName( 'img' );
	for( i = 0; i < imgs.length; i++ ) {
		w = imgs[i].getAttribute( 'width' );
		imgs[i].removeAttribute( 'width' );
		imgs[i].removeAttribute( 'height' );
	}
	});
	</script>
	
	<script src="https://adlc.uad.ac.id/wp-content/themes/ADLC/js/excanvas.js"></script>
	
	<![endif]-->
	
	<!--[if lte IE 9]>
	<script type="text/javascript">
	jQuery(document).ready(function() {
	
	// Combine inline styles for body tag
	jQuery('body').each( function() {	
		var combined_styles = '<style type="text/css">';

		jQuery( this ).find( 'style' ).each( function() {
			combined_styles += jQuery(this).html();
			jQuery(this).remove();
		});

		combined_styles += '</style>';

		jQuery( this ).prepend( combined_styles );
	});
	});
	</script>
	
	<![endif]-->	
	
	<script type="text/javascript">
	/*@cc_on
		@if (@_jscript_version == 10)
			document.write('<style type="text/css">.search input,.searchform input {padding-left:10px;} .avada-select-parent .select-arrow,.select-arrow{height:33px;background-color:#ffffff;}.search input{padding-left:5px;}header .tagline{margin-top:3px;}.star-rating span:before {letter-spacing: 0;}.avada-select-parent .select-arrow,.gravity-select-parent .select-arrow,.wpcf7-select-parent .select-arrow,.select-arrow{background: #fff;}.star-rating{width: 5.2em;}.star-rating span:before {letter-spacing: 0.1em;}</style>');
		@end
	@*/

	var doc = document.documentElement;
	doc.setAttribute('data-useragent', navigator.userAgent);
	</script>

		<style type="text/css">
		ADLC UAD_3.7.3{color:green;}
	
	
		
		html, body { background-color:#d7d6d6; }
	
	
	.header-wrapper .header-social, .sticky-header .sticky-shadow, .tfs-slider .slide-content, #header, .header-v4 #small-nav, .header-v5 #small-nav, #footer, .footer-area, #slidingbar, .page-title-container{ padding-left: 30px; padding-right: 30px; }		
	#main { padding-left: 30px; padding-right: 30px; }
	.width-100 .fullwidth-box, .width-100 .fusion-section-separator {
		padding-left: 30px;
		padding-right: 30px;
	}
	.width-100 .fullwidth-box, .width-100 .fusion-section-separator {
		margin-left: -30px;
		margin-right: -30px;
	}
	/* for full width container with 100% interior checked */
	.width-100 .hundred-percent-fullwidth {
		padding-left: 0px !important; padding-right: 0px !important;
	}

	.mobile-menu-design-modern #mobile-nav li a, .mobile-header-search { padding-left: 30px; padding-right: 30px; }
	
	.mobile-menu-design-modern #mobile-nav li.mobile-nav-item .open-submenu { padding-right: 35px; }			
	.mobile-menu-design-modern #mobile-nav li.mobile-nav-item li a { padding-left: 42px; }
	.mobile-menu-design-modern #mobile-nav li.mobile-nav-item li li a { padding-left: 55px; }
	.mobile-menu-design-modern #mobile-nav li.mobile-nav-item li li li a { padding-left: 68px; }
	.mobile-menu-design-modern #mobile-nav li.mobile-nav-item li li li li a { padding-left: 81px; }		

	.rtl.mobile-menu-design-modern #mobile-nav li.mobile-nav-item .open-submenu { padding-left: 30px; padding-right: 15px; }
	.rtl.mobile-menu-design-modern #mobile-nav li.mobile-nav-item li a { padding-left: 0; padding-right: 42px; }
	.rtl.mobile-menu-design-modern #mobile-nav li.mobile-nav-item li li a { padding-left: 0; padding-right: 55px;	}
	.rtl.mobile-menu-design-modern #mobile-nav li.mobile-nav-item li li li a { padding-left: 0; padding-right: 68px; }
	.rtl.mobile-menu-design-modern #mobile-nav li.mobile-nav-item li li li li a { padding-left: 0; padding-left: 81px; }

		@media only screen and (max-width: 800px) {
		.mobile-menu-design-modern .header-social { padding-left: 0 !important; padding-right: 0 !important; }
		#side-header{width:auto;}
	}
		@media only screen and (max-width: 1024px) {
		.width-100#main { padding-left: 30px !important; padding-right: 30px !important; }
		.width-100 .fullwidth-box, .width-100 .fusion-section-separator {
			padding-left: 30px !important;
			padding-right: 30px !important;
		}
		.width-100 .fullwidth-box, .width-100 .fusion-section-separator {
			margin-left: -30px !important;
			margin-right: -30px !important;
		}
		/* for full width container with 100% interior checked */
		.width-100 .hundred-percent-fullwidth {
			padding-left: 0px !important; padding-right: 0px !important;
		}
	}
		
				
		@media only screen and (min-width: 850px) and (max-width: 930px) {
			.grid-layout-6 .post,
			.portfolio-six .portfolio-item {
				width: 20% !important;
			}

			.grid-layout-5 .post,
			.portfolio-five .portfolio-item {
				width: 25% !important;
			}
		}

		@media only screen and (min-width: 800px) and (max-width: 850px) {
			.grid-layout-6 .post,
			.portfolio-six .portfolio-item {
				width: 25% !important;
			}

			.grid-layout-5 .post,
			.portfolio-five .portfolio-item {
				width: 33.3333333333% !important;
			}

			.grid-layout-4 .post,
			.portfolio-four .portfolio-item {
				width: 33.3333333333% !important;
			}
		}

		@media only screen and (min-width: 700px) and (max-width: 800px) {
			.grid-layout-6 .post,
			.portfolio-six .portfolio-item {
				width: 33.3333333333% !important;
			}

			.grid-layout-5 .post,
			.grid-layout-4 .post,
			.grid-layout-3 .post,
			.portfolio-five .portfolio-item,
			.portfolio-four .portfolio-item,
			.portfolio-three .portfolio-item,
			.portfolio-masonry .portfolio-item {
				width: 50% !important;
			}
		}

		@media only screen and (min-width: 640px) and (max-width: 700px) {
			.grid-layout-6 .post,
			.grid-layout-5 .post,
			.grid-layout-4 .post,
			.grid-layout-3 .post,
			.portfolio-six .portfolio-item,
			.portfolio-five .portfolio-item,
			.portfolio-four .portfolio-item,
			.portfolio-three .portfolio-item,
			.portfolio-masonry .portfolio-item {
				width: 50% !important;
			}
		}

		@media only screen and (max-width: 640px) {
			.grid-layout .post,
			.portfolio-item {
				width: 100% !important;
			}			
		}
		@media only screen and (min-device-width: 768px) and (max-device-width: 1366px) and (orientation: portrait) {
			.grid-layout-6 .post,
			.portfolio-six .portfolio-item {
				width: 33.3333333333% !important;
			}

			.grid-layout-5 .post,
			.grid-layout-4 .post,
			.grid-layout-3 .post,
			.portfolio-five .portfolio-item,
			.portfolio-four .portfolio-item,
			.portfolio-three .portfolio-item,
			.portfolio-masonry .portfolio-item {
				width: 50% !important;
			}
		}
	

	
	/*IE11 hack */
	@media screen and (-ms-high-contrast: active), (-ms-high-contrast: none) {
		.avada-select-parent .select-arrow,.select-arrow, 
		.wpcf7-select-parent .select-arrow{height:33px;line-height:33px;}
		.gravity-select-parent .select-arrow{height:24px;line-height:24px;}
		
		#wrapper .gf_browser_ie.gform_wrapper .button,
		#wrapper .gf_browser_ie.gform_wrapper .gform_footer input.button{ padding: 0 20px; }
	}

	a:hover, .tooltip-shortcode, #mobile-nav li.mobile-nav-item .open-submenu:hover {
	color:#b70101;
}
#nav ul .current_page_item > a, #nav ul .current-menu-item > a, #nav ul > .current-menu-parent > a,
#sticky-nav ul .current_page_item > a, #sticky-nav ul .current-menu-item > a, #sticky-nav ul > .current-menu-parent > a,
.footer-area ul li a:hover,
.footer-area .fusion-tabs-widget .tab-holder .news-list li .post-holder a:hover,
.footer-area .fusion-accordian .panel-title a:hover,
#slidingbar-area ul li a:hover,
#slidingbar-area .fusion-accordian .panel-title a:hover,
.portfolio-tabs li.active a, .faq-tabs li.active a,
.project-content .project-info .project-info-box a:hover,
#main .post h2 a:hover,
#main .about-author .title a:hover,
span.dropcap,.footer-area a:hover,#slidingbar-area a:hover,.copyright a:hover,
.sidebar .widget_categories li a:hover,
.sidebar .widget li a:hover,
#nav ul li > a:hover, #sticky-nav ul li > a:hover,
#nav .cart-contents .cart-link a:hover, #nav .cart-contents .checkout-link a:hover, #nav .cart-contents .cart-link a:hover:before, #nav .cart-contents .checkout-link a:hover:before,
.date-and-formats .format-box i,
h5.toggle:hover a,
.tooltip-shortcode,.content-box-percentage,
.fusion-popover,
.woocommerce .address .edit:hover:after,
.my_account_orders .order-actions a:hover:after,
.more a:hover:after,.read-more:hover:after,.entry-read-more a:hover:after,.pagination-prev:hover:before,.pagination-next:hover:after,.bbp-topic-pagination .prev:hover:before,.bbp-topic-pagination .next:hover:after,
.single-navigation a[rel=prev]:hover:before,.single-navigation a[rel=next]:hover:after,
.sidebar .widget_nav_menu li a:hover:before,.sidebar .widget_categories li a:hover:before,
.sidebar .widget .recentcomments:hover:before,.sidebar .widget_recent_entries li a:hover:before,
.sidebar .widget_archive li a:hover:before,.sidebar .widget_pages li a:hover:before,
.sidebar .widget_links li a:hover:before,.side-nav .arrow:hover:after,.woocommerce-tabs .tabs a:hover .arrow:after,
#wrapper .jtwt .jtwt_tweet a:hover,
.star-rating:before,.star-rating span:before,.price ins .amount, .avada-order-details .shop_table.order_details tfoot tr:last-child .amount,
.price > .amount,.woocommerce-pagination .prev:hover,.woocommerce-pagination .next:hover,.woocommerce-pagination .prev:hover:before,.woocommerce-pagination .next:hover:after,
.woocommerce-tabs .tabs li.active a,.woocommerce-tabs .tabs li.active a .arrow:after,
#wrapper .cart-checkout a:hover,#wrapper .cart-checkout a:hover:before,
.widget_shopping_cart_content .total .amount,.widget_layered_nav li a:hover:before,
.widget_product_categories li a:hover:before,#header-sticky .my-account-link-active:after,#header .my-account-link-active:after,.woocommerce-side-nav li.active a,.woocommerce-side-nav li.active a:after,.my_account_orders .order-number a,.shop_table .product-subtotal .amount,
.cart_totals .order-total .amount,form.checkout .shop_table tfoot .order-total .amount,#final-order-details .mini-order-details tr:last-child .amount,.rtl .more a:hover:before,.rtl .read-more:hover:before,.rtl .entry-read-more a:hover:before,#header-sticky .my-cart-link-active:after,.header-wrapper .my-cart-link-active:after,#wrapper .sidebar .current_page_item > a,#wrapper .sidebar .current-menu-item > a,#wrapper .sidebar .current_page_item > a:before,#wrapper .sidebar .current-menu-item > a:before,#wrapper .footer-area .current_page_item > a,#wrapper .footer-area .current-menu-item > a,#wrapper .footer-area .current_page_item > a:before,#wrapper .footer-area .current-menu-item > a:before,#wrapper #slidingbar-area .current_page_item > a,#wrapper #slidingbar-area .current-menu-item > a,#wrapper #slidingbar-area .current_page_item > a:before,#wrapper #slidingbar-area .current-menu-item > a:before,.side-nav ul > li.current_page_item > a,.side-nav li.current_page_ancestor > a,
.gform_wrapper span.ginput_total,.gform_wrapper span.ginput_product_price,.ginput_shipping_price,
.bbp-topics-front ul.super-sticky a:hover, .bbp-topics ul.super-sticky a:hover, .bbp-topics ul.sticky a:hover, .bbp-forum-content ul.sticky a:hover, .fusion-accordian .panel-title a:hover, #nav .cart-contents .cart-link a:hover:before, #nav .cart-contents .checkout-link a:hover:before{
	color:#b70101;
}
.fusion-content-boxes .heading-link:hover h2 {
	color:#b70101 !important;
}
.fusion-content-boxes .heading-link:hover .icon i, .fusion-accordian .panel-title a:hover .fa-fusion-box {
	background-color: #b70101 !important;
	border-color: #b70101 !important;
}

.sidebar .image .image-extras .image-extras-content a:hover { color: #333333; }
.star-rating:before,.star-rating span:before {
	color:#b70101;
}
.tagcloud a:hover,#slidingbar-area .tagcloud a:hover,.footer-area .tagcloud a:hover{ color: #FFFFFF; text-shadow: none; -moz-text-shadow: none; -webkit-text-shadow: none; }
#nav ul .current_page_item > a, #nav ul .current-menu-item  > a, #nav ul > .current-menu-parent > a, #nav ul .current-menu-ancestor > a, .navigation li.current-menu-ancestor > a,
#sticky-nav ul .current_page_item > a, #sticky-nav ul .current-menu-item > a, #sticky-nav ul > .current-menu-parent > a, #sticky-nav li.current-menu-ancestor > a,
#nav ul ul,#sticky-nav ul ul,
.reading-box,
.portfolio-tabs li.active a, .faq-tabs li.active a,
#wrapper .fusion-tabs-widget .tab-holder .tabs li.active a,
#wrapper .post-content blockquote,
.progress-bar-content,
.pagination .current,
.bbp-topic-pagination .current,
.pagination a.inactive:hover,
.woocommerce-pagination .page-numbers.current,
.woocommerce-pagination .page-numbers:hover,
#wrapper .fusion-megamenu-wrapper .fusion-megamenu-holder,
#nav ul li > a:hover,#sticky-nav ul li > a:hover,.woocommerce-pagination .current,
.tagcloud a:hover,#header-sticky .my-account-link:hover:after,#header .my-account-link:hover:after,body #header-sticky .my-account-link-active:after,body #header .my-account-link-active:after,
#bbpress-forums div.bbp-topic-tags a:hover,
#wrapper .fusion-tabs.classic .nav-tabs > .active > .tab-link:hover, #wrapper .fusion-tabs.classic .nav-tabs > .active > .tab-link:focus, #wrapper .fusion-tabs.classic .nav-tabs > .active > .tab-link,#wrapper .fusion-tabs.vertical-tabs.classic .nav-tabs > li.active > .tab-link{
	border-color:#b70101;
}
#nav ul .current-menu-ancestor > a,.navigation li.current-menu-ancestor > a, #sticky-nav li.current-menu-ancestor > a {
	color: #b70101;
}
#wrapper .side-nav li.current_page_item a{
	border-right-color:#b70101;
	border-left-color:#b70101;
}
.header-v2 .header-social, .header-v3 .header-social, .header-v4 .header-social,.header-v5 .header-social,.header-v2{
	border-top-color:#b70101;
}

.fusion-accordian .panel-title .active .fa-fusion-box,
ul.circle-yes li:before,
.circle-yes ul li:before,
.progress-bar-content,
.pagination .current,
.bbp-topic-pagination .current,
.header-v3 .header-social,.header-v4 .header-social,.header-v5 .header-social,
.date-and-formats .date-box,.table-2 table thead,
.onsale,.woocommerce-pagination .current,
.woocommerce .social-share li a:hover i,
.price_slider_wrapper .ui-slider .ui-slider-range,
.tagcloud a:hover,.cart-loading,
#toTop:hover,
#bbpress-forums div.bbp-topic-tags a:hover,
#wrapper .search-table .search-button input[type="submit"]:hover,
ul.arrow li:before,
p.demo_store,
.avada-myaccount-data .digital-downloads li:before, .avada-thank-you .order_details li:before,
.sidebar .widget_layered_nav li.chosen, .sidebar .widget_layered_nav_filters li.chosen {
	background-color:#b70101;
}
.woocommerce .social-share li a:hover i {
	border-color:#b70101;
}
.bbp-topics-front ul.super-sticky, .bbp-topics ul.super-sticky, .bbp-topics ul.sticky, .bbp-forum-content ul.sticky	{
	background-color: #ffffe8;
	opacity: 1;
}




	#header-sticky .my-cart-link:after, #header-sticky a.search-link:after, #side-header .my-cart-link:after, #side-header a.search-link:after, #header .my-cart-link:after, #header a.search-link:after,
	#small-nav .my-cart-link:after, #small-nav a.search-link:after{ border: none; }
	#side-header .my-cart-link:after, #side-header a.search-link:after{ padding: 0; }
.mobile-nav-holder .mobile-selector, 
.mobile-topnav-holder .mobile-selector, 
#mobile-nav {
	background-color: #f9f9f9}
.mobile-nav-holder .mobile-selector, .mobile-topnav-holder .mobile-selector, #mobile-nav, #mobile-nav li a, #mobile-nav li a:hover, .mobile-nav-holder .mobile-selector .selector-down, .mobile-menu-design-modern .header-wrapper #mobile-nav, .sh-mobile-nav-holder.mobile-nav-holder-modern #mobile-nav,
#mobile-nav li.mobile-current-nav-item > a, .mobile-topnav-holder .mobile-selector .selector-down{ border-color: #dadada; }
.mobile-nav-holder .mobile-selector .selector-down:before, .mobile-menu-icons a, .mobile-menu-icons a:before, .mobile-topnav-holder .mobile-selector .selector-down:before{color:#dadada;}
#mobile-nav li > a:hover,
#mobile-nav li.mobile-current-nav-item > a {
	background-color: #f6f6f6}


body #header-sticky.sticky-header .sticky-shadow{background:rgba(255, 255, 255, 0.97);}
.no-rgba #header-sticky.sticky-header .sticky-shadow{background:#ffffff; filter: progid: DXImageTransform.Microsoft.Alpha(Opacity=97); opacity: 0.97;}

#header,#small-nav,#side-header{
	background-color:#b70101;
	background-color:rgba(183,1,1,1);
}

#main,#wrapper,
.fusion-separator .icon-wrapper, html, body, .bbp-arrow { background-color:#cabea8; }

.footer-area{
	background-color:#756f61;
}
#wrapper .footer-area .fusion-tabs-widget .tab-holder .tabs li {
	border-color:#756f61;
}

.footer-area{
	border-color:#b70101;
}

#footer{
	background-color:#282a2b;
}

#footer{
	border-color:#4b4c4d;
}

.sep-boxed-pricing .panel-heading{
	background-color:#c40606;
	border-color:#c40606;
}
.fusion-pricing-table .panel-body .price .integer-part, .fusion-pricing-table .panel-body .price .decimal-part,
.full-boxed-pricing.fusion-pricing-table .standout .panel-heading h3{
	color:#c40606;
}
.image .image-extras{
	background-image: linear-gradient(top, rgba(232,7,7,0.8) 0%, rgba(214,7,7,0.8) 100%);
	background-image: -o-linear-gradient(top, rgba(232,7,7,0.8) 0%, rgba(214,7,7,0.8) 100%);
	background-image: -moz-linear-gradient(top, rgba(232,7,7,0.8) 0%, rgba(214,7,7,0.8) 100%);
	background-image: -webkit-linear-gradient(top, rgba(232,7,7,0.8) 0%, rgba(214,7,7,0.8) 100%);
	background-image: -ms-linear-gradient(top, rgba(232,7,7,0.8) 0%, rgba(214,7,7,0.8) 100%);

	background-image: -webkit-gradient(
		linear,
		left top,
		left bottom,
		color-stop(0, rgba(232,7,7,0.8)),
		color-stop(1, rgba(214,7,7,0.8))
	);
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#e80707', endColorstr='#d60707')
			progid: DXImageTransform.Microsoft.Alpha(Opacity=0);
}
.no-cssgradients .image .image-extras{
	background:#e80707;
}
.image:hover .image-extras {
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#e80707', endColorstr='#d60707')
 			progid: DXImageTransform.Microsoft.Alpha(Opacity=100);
 }
.portfolio-one .button,
#main .comment-submit,
#reviews input#submit,
.comment-form input[type="submit"],
.wpcf7-form input[type="submit"],.wpcf7-submit,
.bbp-submit-wrapper .button,
.button-default,
.button.default,
.price_slider_amount button,
.gform_wrapper .gform_button,
.woocommerce .single_add_to_cart_button,
.woocommerce button.button,
.woocommerce .shipping-calculator-form .button,
.woocommerce form.checkout #place_order,
.woocommerce .checkout_coupon .button,
.woocommerce .login .button,
.woocommerce .register .button,
.woocommerce .avada-order-details .order-again .button,
.woocommerce .avada-order-details .order-again .button,
.woocommerce .lost_reset_password input[type=submit],
#bbp_user_edit_submit,
.ticket-selector-submit-btn[type=submit],
.gform_page_footer input[type=button]{
	background: #e80707;
	color: #780404;
	
		
	background-image: -webkit-gradient( linear, left bottom, left top, from( #d60707 ), to( #e80707 ) );
	background-image: -webkit-linear-gradient( bottom,#d60707, #e80707 );
	background-image:	-moz-linear-gradient( bottom, #d60707, #e80707 );
	background-image:	  -o-linear-gradient( bottom, #d60707, #e80707 );
	background-image: linear-gradient( to top,#d60707, #e80707 );	

	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#e80707', endColorstr='#d60707');
		
	-webkit-transition: all .2s;
	-moz-transition: all .2s;
	-ms-transition: all .2s;	
	-o-transition: all .2s;
	transition: all .2s;	
}
.no-cssgradients .portfolio-one .button,
.no-cssgradients #main .comment-submit,
.no-cssgradients #reviews input#submit,
.no-cssgradients .comment-form input[type="submit"],
.no-cssgradients .wpcf7-form input[type="submit"],
.no-cssgradients .wpcf7-submit,
.no-cssgradients .bbp-submit-wrapper .button,
.no-cssgradients .button-default,
.no-cssgradients .button.default,
.no-cssgradients .price_slider_amount button,
.no-cssgradients .gform_wrapper .gform_button,
.no-cssgradients .woocommerce .single_add_to_cart_button,
.no-cssgradients .woocommerce button.button,
.no-cssgradients .woocommerce .shipping-calculator-form .button,
.no-cssgradients .woocommerce form.checkout #place_order,
.no-cssgradients .woocommerce .checkout_coupon .button,
.no-cssgradients .woocommerce .login .button,
.no-cssgradients .woocommerce .register .button,
.no-cssgradients .woocommerce .avada-order-details .order-again .button
.no-cssgradients .woocommerce .lost_reset_password input[type=submit],
.no-cssgradients #bbp_user_edit_submit,
.no-cssgradients .ticket-selector-submit-btn[type=submit],
.no-cssgradients .gform_page_footer input[type=button]{
	background:#e80707;
}
.portfolio-one .button:hover,
#main .comment-submit:hover,
#reviews input#submit:hover,
.comment-form input[type="submit"]:hover,
.wpcf7-form input[type="submit"]:hover,.wpcf7-submit:hover,
.bbp-submit-wrapper .button:hover,
.button-default:hover,
.button.default:hover,
.price_slider_amount button:hover,
.gform_wrapper .gform_button:hover,
.woocommerce .single_add_to_cart_button:hover,
.woocommerce .shipping-calculator-form .button:hover,
.woocommerce form.checkout #place_order:hover,
.woocommerce .checkout_coupon .button:hover,
.woocommerce .login .button:hover,
.woocommerce .register .button:hover,
.woocommerce .avada-order-details .order-again .button:hover,
.woocommerce .lost_reset_password input[type=submit]:hover,
#bbp_user_edit_submit:hover,
.ticket-selector-submit-btn[type=submit]:hover,
.gform_page_footer input[type=button]:hover{
	background: #d60707;
	color: #780404;
	
		
	background-image: -webkit-gradient( linear, left bottom, left top, from( #e80707 ), to( #d60707 ) );
	background-image: -webkit-linear-gradient( bottom, #e80707, #d60707 );
	background-image:	-moz-linear-gradient( bottom, #e80707}, #d60707 );
	background-image:	  -o-linear-gradient( bottom, #e80707, #d60707 );
	background-image: linear-gradient( to top, #e80707, #d60707 );

	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#d60707', endColorstr='#e80707');
	}
.no-cssgradients .portfolio-one .button:hover,
.no-cssgradients #main .comment-submit:hover,
.no-cssgradients #reviews input#submit:hover,
.no-cssgradients .comment-form input[type="submit"]:hover,
.no-cssgradients .wpcf7-form input[type="submit"]:hover,
.no-cssgradients .wpcf7-submit:hover,
.no-cssgradients .bbp-submit-wrapper .button:hover,
.no-cssgradients .button-default:hover,
.no-cssgradinets .button.default:hover,
.no-cssgradients .price_slider_amount button:hover,
.no-cssgradients .gform_wrapper .gform_button:hover,
.no-cssgradients .woocommerce .single_add_to_cart_button:hover
.no-cssgradients .woocommerce .shipping-calculator-form .button:hover,
.no-cssgradients .woocommerce form.checkout #place_order:hover,
.no-cssgradients .woocommerce .checkout_coupon .button:hover,
.no-cssgradients .woocommerce .login .button:hover,
.no-cssgradients .woocommerce .register .button:hover,
.no-cssgradients .woocommerce .avada-order-details .order-again .button:hover,
.no-cssgradients .woocommerce .lost_reset_password input[type=submit]:hover,
.no-cssgradients #bbp_user_edit_submit:hover,
.no-cssgradients .ticket-selector-submit-btn[type=submit]:hover,
.no-cssgradients .gform_page_footer input[type=button]:hover{
	background:#d60707;
}

.image .image-extras .image-extras-content .icon.link-icon, .image .image-extras .image-extras-content .icon.gallery-icon { background-color:#333333; }
.image-extras .image-extras-content h3, .image .image-extras .image-extras-content h3 a, .image .image-extras .image-extras-content h4, .image .image-extras .image-extras-content h4 a,.image .image-extras .image-extras-content h3, .image .image-extras .image-extras-content h2, .image .image-extras .image-extras-content a,.image .image-extras .image-extras-content .cats,.image .image-extras .image-extras-content .cats a{ color:#333333; }

.page-title-container{border-color:#d2d3d4;}

.footer-area{
	
		padding-top: 43px;
	
		padding-bottom: 40px;
	}
.footer-area > .avada-row, #footer > .avada-row {
		padding-left: 0px;
	
		padding-right: 0px;
	}



#footer{
		padding-top: 18px;
	
		padding-bottom: 16px;
	}

.fontawesome-icon.circle-yes{
	background-color:#333333;
}

.fontawesome-icon.circle-yes{
	border-color:#333333;
}

.fontawesome-icon,
.avada-myaccount-data .digital-downloads li:before,
.avada-myaccount-data .digital-downloads li:after,
.avada-thank-you .order_details li:before,
.avada-thank-you .order_details li:after,
.post-content .error-menu li:before,
.post-content .error-menu li:after{
	color:#ffffff;
}

.fusion-title .title-sep,.product .product-border{
	border-color:#e0dede;
}

.review blockquote q,.post-content blockquote,form.checkout .payment_methods .payment_box{
	background-color:#f6f6f6;
}
.fusion-testimonials .author:after{
	border-top-color:#f6f6f6;
}

.review blockquote q,.post-content blockquote{
	color:#747474;
}



body, #nav ul li ul li a, #sticky-nav ul li ul li a,
#wrapper .fusion-megamenu-wrapper .fusion-megamenu-widgets-container,
.more,
.avada-container h3,
.meta .date,
.review blockquote q,
.review blockquote div strong,
.image .image-extras .image-extras-content h4,
.image .image-extras .image-extras-content h4 a,
.project-content .project-info h4,
.post-content blockquote,
.button-default, .button-large, .button-small, .button-medium,.button-xlarge,
.button.large, .button.small, .button.medium,.button.xlarge,
.ei-title h3,.cart-contents,
.comment-form input[type="submit"],
.wpcf7-form input[type="submit"],
.gform_wrapper .gform_button,
.woocommerce-success-message .button,
.woocommerce .single_add_to_cart_button,
.woocommerce button.button,
.woocommerce .shipping-calculator-form .button,
.woocommerce form.checkout #place_order,
.woocommerce .checkout_coupon .button,
.woocommerce .login .button,
.woocommerce .register .button,
.page-title h3,
.blog-shortcode h3.timeline-title,
#reviews #comments > h2,
.image .image-extras .image-extras-content h3,
.image .image-extras .image-extras-content h2,
.image .image-extras .image-extras-content a,
.image .image-extras .image-extras-content .cats,
.image .image-extras .image-extras-content .cats a,
.image .image-extras .image-extras-content .price,
#wrapper #nav ul li ul li > a, #wrapper #sticky-nav ul li ul li > a,
#bbp_user_edit_submit,
.ticket-selector-submit-btn[type=submit],
.gform_page_footer input[type=button]{
	font-family:'Lucida Sans Unicode', 'Lucida Grande', sans-serif;
}

.avada-container h3,
.review blockquote div strong,
.footer-area  h3,
#slidingbar-area  h3,
.button-default, .button-large, .button-small, .button-medium,.button-xlarge,
.button.large, .button.small, .button.medium,.button.xlarge,
.woocommerce .single_add_to_cart_button,
.woocommerce button.button,
.woocommerce .shipping-calculator-form .button,
.woocommerce form.checkout #place_order,
.woocommerce .checkout_coupon .button,
.woocommerce .login .button,
.woocommerce .register .button,
.woocommerce .avada-order-details .order-again .button,
.comment-form input[type="submit"],
.wpcf7-form input[type="submit"],
.gform_wrapper .gform_button,
#bbp_user_edit_submit,
.ticket-selector-submit-btn[type=submit],
.gform_page_footer input[type=button]{
	font-weight:bold;
}
.meta .date,
.review blockquote q,
.post-content blockquote{
	font-style:italic;
}


#nav, #sticky-nav, .navigation,
.side-nav li a{
	font-family:'Lucida Sans Unicode', 'Lucida Grande', sans-serif;
}

#main .reading-box h2,
#main h2,
.page-title h1,
.image .image-extras .image-extras-content h3,.image .image-extras .image-extras-content h3 a,
#main .post h2,
.sidebar .widget h3,
#wrapper .fusion-tabs-widget .tab-holder .tabs li a,
.share-box h4,
.project-content h3,
.author .author_title,
.fusion-pricing-table .title-row,
.fusion-pricing-table .pricing-row,
.fusion-person .person-desc .person-author .person-author-wrapper,
.fusion-accordian .panel-title,
.fusion-accordian .panel-heading a,
.fusion-tabs .nav-tabs  li .tab-link,
.post-content h1, .post-content h2, .post-content h3, .post-content h4, .post-content h5, .post-content h6,
.ei-title h2, #header-sticky,#header .tagline,
table th,.project-content .project-info h4,
.woocommerce-success-message .msg,.product-title, .cart-empty,
#wrapper .fusion-megamenu-wrapper .fusion-megamenu-title,
.main-flex .slide-content h2, .main-flex .slide-content h3,
.fusion-modal .modal-title, .popover .popover-title,
.fusion-flip-box .flip-box-heading-back{
	font-family:'Lucida Sans Unicode', 'Lucida Grande', sans-serif;
}


.footer-area  h3,#slidingbar-area  h3{
	font-family:'Lucida Sans Unicode', 'Lucida Grande', sans-serif;
}

body,.sidebar .slide-excerpt h2, .footer-area .slide-excerpt h2,#slidingbar-area .slide-excerpt h2,
.jtwt .jtwt_tweet, .sidebar .jtwt .jtwt_tweet {
	font-size:13px;
		line-height:20px;
}
.project-content .project-info h4,.gform_wrapper label,.gform_wrapper .gfield_description,
.footer-area ul, #slidingbar-area ul, .fusion-tabs-widget .tab-holder .news-list li .post-holder a,
.fusion-tabs-widget .tab-holder .news-list li .post-holder .meta{
	font-size:13px;
		line-height:20px;
}
.blog-shortcode h3.timeline-title { font-size:13px;line-height:13px; }
.counter-box-content, .fusion-alert,.fusion-progressbar .sr-only, .post-content blockquote, .review blockquote q{ font-size:13px; }

body,.sidebar .slide-excerpt h2, .footer-area .slide-excerpt h2,#slidingbar-area .slide-excerpt h2,.post-content blockquote, .review blockquote q{
	line-height:20px;
}
.project-content .project-info h4,.fusion-accordian .panel-body, #side-header .header-social .header-info, #side-header .header-social .top-menu {
	line-height:20px;
}

#nav,#sticky-nav,.navigation{font-size:15px;}

#small-nav .cart, #small-nav .search-link{font-size:14px;}

#wrapper #nav ul li ul li > a, #wrapper #sticky-nav ul li ul li > a{font-size:12px;}

.header-social *{font-size:12px;}

.page-title ul,.page-title ul li,page-title ul li a{font-size:10px;}

.side-nav li a{font-size:14px;}

.sidebar .widget h3{font-size:13px;}

#slidingbar-area h3{font-size:13px;line-height:13px;}

.footer-area h3{font-size:13px;line-height:13px;}

.copyright{font-size:12px;}

#wrapper .fusion-megamenu-wrapper .fusion-megamenu-title{font-size:18px;}


#header-sticky .avada-row,#header .avada-row, #main .avada-row, .footer-area .avada-row,#slidingbar-area .avada-row, #footer .avada-row, .page-title, .header-social .avada-row, #small-nav .avada-row, .tfs-slider .slide-content-container .slide-content{ max-width:1024px; }


.post-content h1{
	font-size:34px;
		line-height:51px;
}

.post-content h1{
	line-height:48px;
}

#wrapper .post-content h2,#wrapper .fusion-title h2,#wrapper #main .post-content .fusion-title h2,#wrapper .title h2,#wrapper #main .post-content .title h2,#wrapper  #main .post h2, #wrapper  #main .post h2, #wrapper .woocommerce .checkout h3, #main .portfolio h2 {
	font-size:18px;
		line-height:27px;
}

#wrapper .post-content h2,#wrapper .fusion-title h2,#wrapper #main .post-content .fusion-title h2,#wrapper .title h2,#wrapper #main .post-content .title h2,#wrapper #main .post h2,#wrapper  .woocommerce .checkout h3, .cart-empty{
	line-height:27px;
}

.post-content h3,.project-content h3,#header .tagline,.product-title,#side-header .tagline{
	font-size:16px;
		line-height:24px;
}
p.demo_store,.fusion-modal .modal-title { font-size:16px; }

.post-content h3,.project-content h3,#header .tagline,.product-title,#side-header .tagline{
	line-height:24px;
}

.post-content h4, .portfolio-item .portfolio-content h4, .image-extras .image-extras-content h3, .image-extras .image-extras-content h3 a, .image .image-extras .image-extras-content a,
.fusion-person .person-author-wrapper .person-name, .fusion-person .person-author-wrapper .person-title
{
	font-size:13px;
		line-height:20px;
}
#wrapper .fusion-tabs-widget .tab-holder .tabs li a,.person-author-wrapper, #reviews #comments > h2,
.popover .popover-title,.fusion-flip-box .flip-box-heading-back{
	font-size:13px;
}
.fusion-accordian .panel-title a,.fusion-sharing-box h4,
.fusion-tabs .nav-tabs > li .tab-link
{font-size:13px;}

.post-content h4, #reviews #comments > h2,
.fusion-sharing-box h4,
.fusion-person .person-author-wrapper .person-name, .fusion-person .person-author-wrapper .person-title {
	line-height:20px;
}

.post-content h5{
	font-size:12px;
		line-height:18px;
}

.post-content h5{
	line-height:18px;
}

.post-content h6{
	font-size:11px;
		line-height:17px;
}

.post-content h6{
	line-height:17px;
}

.ei-title h2{
	font-size:42px;
		line-height:63px;
}

.ei-title h3{
	font-size:20px;
		line-height:30px;
}

.image .image-extras .image-extras-content h4, .image .image-extras .image-extras-content h4 a, .image .image-extras .image-extras-content .cats, .image .image-extras .image-extras-content .cats a, .fusion-recent-posts .columns .column .meta {
	font-size:12px;
		line-height:18px;
}
.post .meta-info, .fusion-blog-grid .entry-meta-single, .fusion-blog-timeline .entry-meta-single, .fusion-blog-grid .entry-comments, .fusion-blog-timeline .entry-comments, .fusion-blog-grid .entry-read-more, .fusion-blog-timeline .entry-read-more, .fusion-blog-medium .entry-meta, .fusion-blog-large .entry-meta, .fusion-blog-medium-alternate .entry-meta, .fusion-blog-large-alternate .entry-meta, .fusion-blog-medium-alternate .entry-read-more, .fusion-blog-large-alternate .entry-read-more, .fusion-recent-posts .columns .column .meta, .post .single-line-meta { font-size:12px; }

.cart-contents *, .top-menu .cart-content a .cart-title, .top-menu .cart-content a .quantity, .image .image-extras .image-extras-content .product-buttons a, .product-buttons a, #header-sticky .cart-content a .cart-title, #header-sticky .cart-content a .quantity, #header .cart-content a .cart-title, #header .cart-content a .quantity, .sticky-header #sticky-nav .cart-checkout a, #header .cart-checkout a {
	font-size:12px;
		line-height:18px;
}

.pagination, .page-links, .woocommerce-pagination, .pagination .pagination-next, .woocommerce-pagination .next, .pagination .pagination-prev, .woocommerce-pagination .prev { font-size:12px; }

.header-social .menu > li {
	line-height:44px;
}
.header-wrapper .header-social .menu > li {
	height:44px;
}

body,.post .post-content,.post-content blockquote,#wrapper .fusion-tabs-widget .tab-holder .news-list li .post-holder .meta,.sidebar .jtwt,#wrapper .meta,.review blockquote div,.search input,.project-content .project-info h4,.title-row,.simple-products-slider .price .amount,
.quantity .qty,.quantity .minus,.quantity .plus,.timeline-layout h3.timeline-title, .blog-timeline-layout h3.timeline-title, #reviews #comments > h2,
.sidebar .widget_nav_menu li, .sidebar .widget_categories li, .sidebar .widget_product_categories li, .sidebar .widget_meta li, .sidebar .widget .recentcomments, .sidebar .widget_recent_entries li, .sidebar .widget_archive li, .sidebar .widget_pages li, .sidebar .widget_links li, .sidebar .widget_layered_nav li, .sidebar .widget_product_categories li
{color:#000000;}

.post-content h1,.title h1,.woocommerce-success-message .msg, .woocommerce-message{
	color:#333333;
}

#main .post h2,.post-content h2,.fusion-title h2,.title h2,.woocommerce-tabs h2,.search-page-search-form h2, .cart-empty, .woocommerce h2, .woocommerce .checkout h3{
	color:#333333;
}

.post-content h3,.sidebar .widget h3,.project-content h3,.fusion-title h3,.title h3,#header .tagline,.person-author-wrapper span,.product-title,#side-header .tagline{
	color:#333333;
}

.post-content h4,.project-content .project-info h4,.share-box h4,.fusion-title h4,.title h4,#wrapper .fusion-tabs-widget .tab-holder .tabs li a, .fusion-accordian .panel-title a,
.fusion-tabs .nav-tabs > li .tab-link
{
	color:#333333;
}

.post-content h5,.fusion-title h5,.title h5{
	color:#333333;
}

.post-content h6,.fusion-title h6,.title h6{
	color:#333333;
}

.page-title h1, .page-title h3{
		color:#333333;
	}

.sep-boxed-pricing .panel-heading h3{
	color:#333333;
}

.full-boxed-pricing.fusion-pricing-table .panel-heading h3{
	color:#333333;
}

body a,
body a:before,
body a:after,
.single-navigation a[rel="prev"]:before,
.single-navigation a[rel="next"]:after
{color:#333333;}
.project-content .project-info .project-info-box a,.sidebar .widget li a, .sidebar .widget .recentcomments, .sidebar .widget_categories li, #main .post h2 a, .about-author .title a,
.shop_attributes tr th,.image-extras a,.products-slider .price .amount,z.my_account_orders thead tr th,.shop_table thead tr th,.cart_totals table th,form.checkout .shop_table tfoot th,form.checkout .payment_methods label,#final-order-details .mini-order-details th,#main .product .product_title,.shop_table.order_details tr th,
.sidebar .widget_layered_nav li.chosen a, .sidebar .widget_layered_nav li.chosen a:before,.sidebar .widget_layered_nav_filters li.chosen a, .sidebar .widget_layered_nav_filters li.chosen a:before,
.order-dropdown li a:hover, .catalog-ordering .order li a:hover
{color:#333333;}

body #toTop:before {color:#fff;}

.page-title ul,.page-title ul li,.page-title ul li a{color:#333333;}

#slidingbar-area h3{color:#DDDDDD;}

#slidingbar-area,#slidingbar-area article.col,#slidingbar-area .jtwt,#slidingbar-area .jtwt .jtwt_tweet{color:#8C8989;}

#slidingbar-area a, #slidingbar-area .jtwt .jtwt_tweet a, #wrapper #slidingbar-area .fusion-tabs-widget .tab-holder .tabs li a, #slidingbar-area .fusion-accordian .panel-title a{color:#BFBFBF;}

.sidebar .widget h3, .sidebar .widget .heading h3{color:#ffffff;}

.footer-area h3{color:#DDDDDD;}

.footer-area,.footer-area article.col,.footer-area .jtwt,.footer-area .jtwt .jtwt_tweet,.copyright{color:#fff9f9;}

.footer-area a,.footer-area .jtwt .jtwt_tweet a,#wrapper .footer-area .fusion-tabs-widget .tab-holder .tabs li a,.footer-area .fusion-tabs-widget .tab-holder .news-list li .post-holder a,.copyright a,
.footer-area .fusion-accordian .panel-title a{color:#ffffff;}

#nav ul li > a,#sticky-nav ul li > a,.side-nav li a,#header-sticky .cart-content a,#header-sticky .cart-content a:hover,#header .cart-content a,#header .cart-content a:hover, #side-header .cart-content a:hover,#small-nav .cart-content a,
#small-nav .cart-content a:hover,#wrapper .header-social .top-menu .cart > a,#wrapper .header-social .top-menu .cart > a > .amount, #wrapper .fusion-megamenu-wrapper .fusion-megamenu-title,#wrapper .fusion-megamenu-wrapper .fusion-megamenu-title a,.my-cart-link:after,a.search-link:after, .top-menu .cart > a:before, .top-menu .cart > a:after
{color:#b70101;}
#header-sticky .my-account-link:after, #header .my-account-link:after, #side-header .my-account-link:after{border-color:#b70101;}

#nav ul .current-menu-ancestor > a, .navigation li.current-menu-ancestor > a, #sticky-nav li.current-menu-ancestor > a,#nav ul .current_page_item > a, #nav ul .current-menu-item > a, #nav ul > .current-menu-parent > a, #nav ul ul,#wrapper .fusion-megamenu-wrapper .fusion-megamenu-holder,.navigation li.current-menu-ancestor > a,#nav ul li > a:hover,
#sticky-nav ul .current_page_item > a, #sticky-nav ul .current-menu-item > a, #sticky-nav ul > .current-menu-parent > a, #sticky-nav ul ul,#sticky-nav li.current-menu-ancestor > a,.navigation li.current-menu-ancestor > a,#sticky-nav ul li > a:hover,
#header-sticky .my-cart-link-active:after, .header-wrapper .my-cart-link-active:after, #side-header .my-cart-link-active:after
{color:#b70101;border-color:#b70101;}

#nav ul ul,#sticky-nav ul ul{border-color:#b70101;}
#wrapper .main-nav-search .search-link:hover:after, #wrapper .main-nav-search.search-box-open .search-link:after, #wrapper .my-cart-link:hover:after {color:#b70101;}

#nav ul ul,#sticky-nav ul ul,
#wrapper .fusion-megamenu-wrapper .fusion-megamenu-holder .fusion-megamenu-submenu,
#nav ul .login-box,#sticky-nav ul .login-box,
#nav ul .cart-contents,#sticky-nav ul .cart-contents,
#small-nav ul .login-box,#small-nav ul .cart-contents,
#main-nav-search-form, #sticky-nav-search-form
{background-color:#f2efef;}

#wrapper #nav ul li ul li > a,#wrapper #sticky-nav ul li ul li > a,.side-nav li li a,.side-nav li.current_page_item li a, #nav .cart-contents a, #nav .cart-contents .cart-link a:before, #nav .cart-contents .checkout-link a:before, #nav .cart-contents a:hover
{color:#333333;}
#wrapper .fusion-megamenu-wrapper .fusion-megamenu-bullet, .fusion-megamenu-bullet{border-left-color:#333333;}

.ei-title h2{color:#333333;}

.ei-title h3{color:#747474;}

#wrapper .header-social .header-info, #wrapper .header-social a {color:#ffffff;}
@media only screen and (max-width: 800px){
	.mobile-menu-design-modern #wrapper .header-social .top-menu .cart > a, .mobile-menu-design-modern #wrapper .header-social .top-menu .cart > a:before {color:#ffffff;}
}
#wrapper .header-social .mobile-topnav-holder li a {color: #333333;}

.header-social .menu .sub-menu {width:100px;}


.sep-single,.sep-double,.sep-dashed,.sep-dotted,.search-page-search-form{border-color:#e0dede;}
.ls-avada, .avada-skin-rev,.es-carousel-wrapper.fusion-carousel-small .es-carousel ul li img,.fusion-accordian .fusion-panel,.progress-bar,
#small-nav,.portfolio-tabs,.faq-tabs,.single-navigation,.project-content .project-info .project-info-box,
.fusion-blog-medium-alternate .post, .fusion-blog-large-alternate .post,
.post .meta-info,.grid-layout .post .post-wrapper,.grid-layout .post .content-sep, 
.portfolio .portfolio-boxed .portfolio-item-wrapper, .portfolio .portfolio-boxed .content-sep, .portfolio-one .portfolio-item.portfolio-boxed .portfolio-item-wrapper,
.grid-layout .post .flexslider,.timeline-layout .post,.timeline-layout .post .content-sep,
.timeline-layout .post .flexslider,h3.timeline-title,.timeline-arrow,
.fusion-counters-box .fusion-counter-box .counter-box-border, tr td,
.table, .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td,
.table-1 table,.table-1 table th,.table-1 tr td,.tkt-slctr-tbl-wrap-dv table,.tkt-slctr-tbl-wrap-dv tr td
.table-2 table thead,.table-2 tr td,
.sidebar .widget li a,.sidebar .widget .recentcomments,.sidebar .widget_categories li,
#wrapper .fusion-tabs-widget .tab-holder,.commentlist .the-comment,
.side-nav,#wrapper .side-nav li a,.rtl .side-nav,h5.toggle.active + .toggle-content,
#wrapper .side-nav li.current_page_item li a,.tabs-vertical .tabset,
.tabs-vertical .tabs-container .tab_content,
.fusion-tabs.vertical-tabs.clean .nav-tabs li .tab-link,
.pagination a.inactive, .page-links a,.woocommerce-pagination .page-numbers,.bbp-topic-pagination .page-numbers,.rtl .woocommerce .social-share li,.author .author_social, .fusion-blog-medium .entry-meta, .fusion-blog-large .entry-meta,
.side-nav li a,.sidebar .product_list_widget li,.sidebar .widget_layered_nav li,.price_slider_wrapper,.tagcloud a,
.sidebar .widget_nav_menu li, .sidebar .widget_categories li, .sidebar .widget_product_categories li, .sidebar .widget_meta li, .sidebar .widget .recentcomments, .sidebar .widget_recent_entries li, .sidebar .widget_archive li, .sidebar .widget_pages li, .sidebar .widget_links li, .sidebar .widget_layered_nav li, .sidebar .widget_product_categories li,
#customer_login_box,.avada_myaccount_user,#wrapper .myaccount_user_container span,
.woocommerce-side-nav li a,.woocommerce-content-box,.woocommerce-content-box h2,.my_account_orders tr,.woocommerce .address h4,.shop_table tr,.cart_totals .total,.chzn-container-single .chzn-single,.chzn-container-single .chzn-single div,.chzn-drop,form.checkout .shop_table tfoot,.input-radio,p.order-info,.cart-content a img,.panel.entry-content,
.woocommerce-tabs .tabs li a,.woocommerce .social-share,.woocommerce .social-share li,.quantity,.quantity .minus, .quantity .qty,.shop_attributes tr,.woocommerce-success-message,#reviews li .comment-text,
.cart-totals-buttons,.cart_totals, .shipping_calculator, .coupon, .woocommerce .cross-sells, #customer_login .col-1, #customer_login .col-2, .woocommerce-message, .woocommerce form.checkout #customer_details .col-1, .woocommerce form.checkout #customer_details .col-2,
.cart_totals h2, .shipping_calculator h2, .coupon h2, .woocommerce .checkout h3, #customer_login h2, .woocommerce .cross-sells h2, .order-total, .woocommerce .addresses .title, #main .cart-empty, #main .return-to-shop, .side-nav-left .side-nav,
.avada-order-details .shop_table.order_details tfoot
{border-color:#e0dede;}
#final-order-details .mini-order-details tr:last-child
{border-color:#e0dede;}

.price_slider_wrapper .ui-widget-content
{background-color:#e0dede;}
.gform_wrapper .gsection{border-bottom:1px dotted #e0dede;}

.quantity .minus,.quantity .plus{background-color:#fbfaf9;}

.quantity .minus:hover,.quantity .plus:hover{background-color:#ffffff;}

#slidingbar-area .widget_categories li a, #slidingbar-area li.recentcomments, #slidingbar-area ul li a, #slidingbar-area .product_list_widget li, #slidingbar-area .widget_recent_entries ul li {border-bottom: 1px solid #282A2B;}
#slidingbar-area .tagcloud a, 
#wrapper #slidingbar-area .fusion-tabs-widget .tab-holder, 
#wrapper #slidingbar-area .fusion-tabs-widget .tab-holder .news-list li,
#slidingbar-area .fusion-accordian .fusion-panel
{border-color: #282A2B;}

.footer-area .widget_categories li a, .footer-area li.recentcomments, .footer-area ul li a, .footer-area .product_list_widget li, .footer-area .tagcloud a,
#wrapper .footer-area .fusion-tabs-widget .tab-holder, 
#wrapper .footer-area .fusion-tabs-widget .tab-holder .news-list li, 
.footer-area .widget_recent_entries li,
.footer-area .fusion-accordian .fusion-panel
{border-color: #505152;}

.input-text, input[type="text"], textarea,
input.s,#comment-input input,#comment-textarea textarea,.comment-form-comment textarea, .post-password-form .password,
.wpcf7-form .wpcf7-text,.wpcf7-form .wpcf7-quiz,.wpcf7-form .wpcf7-number,.wpcf7-form textarea,.wpcf7-form .wpcf7-select,.wpcf7-captchar,.wpcf7-form .wpcf7-date,
.gform_wrapper .gfield input[type=text],.gform_wrapper .gfield input[type=email],.gform_wrapper .gfield textarea,.gform_wrapper .gfield select,
#bbpress-forums .bbp-search-form #bbp_search,.bbp-reply-form input#bbp_topic_tags,.bbp-topic-form input#bbp_topic_title, .bbp-topic-form input#bbp_topic_tags, .bbp-topic-form select#bbp_stick_topic_select, .bbp-topic-form select#bbp_topic_status_select,#bbpress-forums div.bbp-the-content-wrapper textarea.bbp-the-content,.bbp-login-form input,
.main-nav-search-form input,.search-page-search-form input,.chzn-container-single .chzn-single,.chzn-container .chzn-drop,
.avada-select-parent select,.avada-select-parent .select-arrow, #wrapper .select-arrow,
select,
#lang_sel_click a.lang_sel_sel,
#lang_sel_click ul ul a, #lang_sel_click ul ul a:visited,
#lang_sel_click a, #lang_sel_click a:visited,#wrapper .search-table .search-field input{
background-color:#ffffff;}

.input-text, input[type="text"], textarea,
input.s,input.s .placeholder,#comment-input input,#comment-textarea textarea,#comment-input .placeholder,#comment-textarea .placeholder,.comment-form-comment textarea, .post-password-form .password,
.wpcf7-form .wpcf7-text,.wpcf7-form .wpcf7-quiz,.wpcf7-form .wpcf7-number,.wpcf7-form textarea,.wpcf7-form .wpcf7-select,.wpcf7-select-parent .select-arrow,.wpcf7-captchar,.wpcf7-form .wpcf7-date,
.gform_wrapper .gfield input[type=text],.gform_wrapper .gfield input[type=email],.gform_wrapper .gfield textarea,.gform_wrapper .gfield select,
select,
#bbpress-forums .bbp-search-form #bbp_search,.bbp-reply-form input#bbp_topic_tags,.bbp-topic-form input#bbp_topic_title, .bbp-topic-form input#bbp_topic_tags, .bbp-topic-form select#bbp_stick_topic_select, .bbp-topic-form select#bbp_topic_status_select,#bbpress-forums div.bbp-the-content-wrapper textarea.bbp-the-content,.bbp-login-form input,
.main-nav-search-form input,.search-page-search-form input,.chzn-container-single .chzn-single,.chzn-container .chzn-drop,.avada-select-parent select, #wrapper .search-table .search-field input
{color:#aaa9a9;}
input#s::-webkit-input-placeholder,#comment-input input::-webkit-input-placeholder,.post-password-form .password::-webkit-input-placeholder,#comment-textarea textarea::-webkit-input-placeholder,.comment-form-comment textarea::-webkit-input-placeholder,.input-text::-webkit-input-placeholder{color:#aaa9a9;}
input#s:-moz-placeholder,#comment-input input:-moz-placeholder,.post-password-form .password::-moz-input-placeholder,#comment-textarea textarea:-moz-placeholder,.comment-form-comment textarea:-moz-placeholder,.input-text:-moz-placeholder,
input#s:-ms-input-placeholder,#comment-input input:-ms-input-placeholder,.post-password-form .password::-ms-input-placeholder,#comment-textarea textarea:-moz-placeholder,.comment-form-comment textarea:-ms-input-placeholder,.input-text:-ms-input-placeholder
{color:#aaa9a9;}

.input-text, input[type="text"], textarea,
input.s,#comment-input input,#comment-textarea textarea,.comment-form-comment textarea, .post-password-form .password,
.wpcf7-form .wpcf7-text,.wpcf7-form .wpcf7-quiz,.wpcf7-form .wpcf7-number,.wpcf7-form textarea,.wpcf7-form .wpcf7-select,.wpcf7-select-parent .select-arrow,.wpcf7-captchar,.wpcf7-form .wpcf7-date,
.gform_wrapper .gfield input[type=text],.gform_wrapper .gfield input[type=email],.gform_wrapper .gfield textarea,.gform_wrapper .gfield_select[multiple=multiple],.gform_wrapper .gfield select,.gravity-select-parent .select-arrow,.select-arrow,
#bbpress-forums .quicktags-toolbar,#bbpress-forums .bbp-search-form #bbp_search,.bbp-reply-form input#bbp_topic_tags,.bbp-topic-form input#bbp_topic_title, .bbp-topic-form input#bbp_topic_tags, .bbp-topic-form select#bbp_stick_topic_select, .bbp-topic-form select#bbp_topic_status_select,#bbpress-forums div.bbp-the-content-wrapper textarea.bbp-the-content,#wp-bbp_topic_content-editor-container,#wp-bbp_reply_content-editor-container,.bbp-login-form input,
.main-nav-search-form input,.search-page-search-form input,.chzn-container-single .chzn-single,.chzn-container .chzn-drop,
.avada-select-parent select,.avada-select-parent .select-arrow,
select,
#lang_sel_click a.lang_sel_sel,
#lang_sel_click ul ul a, #lang_sel_click ul ul a:visited,
#lang_sel_click a, #lang_sel_click a:visited,
#wrapper .search-table .search-field input,
.avada-select .select2-container .select2-choice, .woocommerce-checkout .select2-drop-active
{border-color:#d2d2d2;}


#wrapper #nav ul li ul li > a,#wrapper #sticky-nav ul li ul li > a,
#header-sticky .cart-content a,#header .cart-content a,#small-nav .cart-content a
{border-bottom:1px solid #dcdadb;}

#wrapper .fusion-megamenu-wrapper .fusion-megamenu-submenu,
#wrapper #nav .fusion-megamenu-wrapper .fusion-megamenu-border,
#nav .fusion-navbar-nav .fusion-megamenu-wrapper ul ul, #sticky-nav .fusion-navbar-nav .fusion-megamenu-wrapper ul ul,
#wrapper #nav .fusion-navbar-nav .fusion-megamenu-wrapper ul ul li, #wrapper #sticky-nav .fusion-navbar-nav .fusion-megamenu-wrapper ul ul li,
#header-sticky .cart-content a,#header-sticky .cart-content a:hover,#header-sticky .login-box,#header-sticky .cart-contents,
#header .cart-content a, #side-header .cart-content a, #header .cart-content a:hover,#side-header .cart-content a:hover,#header .login-box,#header .cart-contents, #side-header .cart-contents,#small-nav .login-box,#small-nav .cart-contents,#small-nav .cart-content a,#small-nav .cart-content a:hover,
#main-nav-search-form, #sticky-nav-search-form
{border-color:#dcdadb;}

#wrapper #nav ul li ul li > a:hover, #wrapper #nav ul li ul li.current-menu-item > a,
#wrapper .fusion-navbar-nav > li .sub-menu .current-menu-ancestor,
#wrapper #sticky-nav ul li ul li > a:hover, #wrapper #sticky-nav ul li ul li.current-menu-item > a,
#header-sticky .cart-content a:hover,#header .cart-content a:hover,#side-header .cart-content a:hover,#small-nav .cart-content a:hover,
#lang_sel_click a.lang_sel_sel:hover,
#lang_sel_click ul ul a:hover,
#lang_sel_click a:hover
{background-color:#f8f8f8;}

#header .tagline,#side-header .tagline{
	color:#747474;
}

#header .tagline,#side-header .tagline{
	font-size:16px;
	line-height:30px;
}

#wrapper .page-title h1{
		font-size:18px;
		line-height:normal;
}

#wrapper .page-title h3{
		font-size:14px;
		line-height: 26px;
}

.header-social,.header-v2 #header,.header-v3 #header,.header-v4 #header, .header-v5 #header, #header,.header-v4 #small-nav,.header-v5 #small-nav, .header-social .alignleft{
	border-bottom-color:#6d0101;
}
#side-header{border-color:#6d0101;}
#side-header #nav.nav-holder .navigation > li > a{border-top-color:#6d0101;border-bottom-color:#6d0101;}

#nav ul ul,#sticky-nav ul ul{
	width:170px;
}

#nav ul ul li ul,#sticky-nav ul ul li ul{
	left:170px;
}
ul.navigation > li:last-child ul ul{
	left:-170px;
}
#nav .fusion-megamenu-wrapper ul{left:auto;}
.rtl #header #nav ul ul li:hover ul, .rtl #small-nav #nav ul ul li:hover ul, .rtl .sticky-header #sticky-nav ul ul li:hover ul {
	right:170px;
	left: auto;
}
.rtl ul.navigation > li:last-child ul ul{
	right:-170px;
	left: auto;
}
.rtl #wrapper #nav .fusion-megamenu-wrapper ul,
.rtl #wrapper #header-sticky #nav .fusion-megamenu-wrapper ul{
	left:auto;
	right:auto;
}
body.side-header-left #side-header #nav ul .sub-menu li ul {
	left:170px;
}
body.side-header-right #side-header #nav > ul .sub-menu {
	left:-170px;
}

#main .sidebar{
	background-color:#ffffff;
}


#content{
	width:74%;
}

#main .sidebar{
	width:25%;
}


.double-sidebars #content {
	width:73%;
	margin-left: 28%;
}

.double-sidebars #main #sidebar{
	width:25%;
	margin-left:-101%;
}
.double-sidebars #main #sidebar-2{
	width:25%;
	margin-left: 3%;
}

#wrapper .header-social{
	background-color:#8c0001;
}

#wrapper .header-social .menu > li, .mobile-menu-sep{
	border-color:#ffffff;
}

#wrapper .header-social .menu .sub-menu,#wrapper .header-social .login-box,#wrapper .header-social .cart-contents,.main-nav-search-form{
	background-color:#ffffff;
}

#wrapper .header-social .menu .sub-menu li, #wrapper .header-social .menu .sub-menu li a,#wrapper .header-social .login-box .forgetmenot, #wrapper .top-menu .cart-contents a, .top-menu .cart-contents .cart-link a:before, .top-menu .cart-contents .checkout-link a:before{
	color:#747474;
}

#wrapper .header-social .menu .sub-menu li a:hover,.top-menu .cart-content a:hover {
	background-color:#fafafa;
}

#wrapper .header-social .menu .sub-menu li a:hover, #wrapper .top-menu .cart-contents a:hover, #wrapper .top-menu .cart-contents .cart-link a:hover:before, #wrapper .top-menu .cart-contents .checkout-link a:hover:before{
	color:#333333;
}

#wrapper .header-social .menu .sub-menu,#wrapper .header-social .menu .sub-menu li,.top-menu .cart-content a,#wrapper .header-social .login-box,#wrapper .header-social .cart-contents,.main-nav-search-form{
	border-color:#e5e5e5;
}

#header-sticky .cart-checkout,#header .cart-checkout,.top-menu .cart,.top-menu .cart-checkout,#small-nav .cart-checkout{
	background-color:#fafafa;
}

.fusion-accordian .panel-title a .fa-fusion-box{background-color:#333333;}

.progress-bar-content{background-color:#e10707;border-color:#e10707;}
.content-box-percentage{color:#e10707;}

.progress-bar{background-color:#f6f6f6;border-color:#f6f6f6;}

#wrapper .date-and-formats .format-box{background-color:#ffffff;}

.es-nav-prev,.es-nav-next{background-color:#999999;}

.es-nav-prev:hover,.es-nav-next:hover{background-color:#808080;}

.content-boxes .col{background-color:#ffffff;}

#wrapper .sidebar .fusion-tabs-widget .tabs-container{background-color:#ffffff;}
body .sidebar .fusion-tabs-widget .tab-hold .tabs li{border-right:1px solid #ffffff;}
body.rtl #wrapper .sidebar .fusion-tabs-widget .tab-hold .tabset li{border-left-color:#ffffff;}
body .sidebar .fusion-tabs-widget .tab-holder .tabs li a, .sidebar .fusion-tabs-widget .tab-holder .tabs li a{background:#ebeaea;border-bottom:0;color:#000000;}
body .sidebar .fusion-tabs-widget .tab-hold .tabs li a:hover{background:#ffffff;border-bottom:0;}
body .sidebar .fusion-tabs-widget .tab-hold .tabs li.active a, body .sidebar .fusion-tabs-widget .tab-holder .tabs li.active a{background:#ffffff;border-bottom:0;}
body .sidebar .fusion-tabs-widget .tab-hold .tabs li.active a, body .sidebar .fusion-tabs-widget .tab-holder .tabs li.active a{border-top-color:#b70101;}

#wrapper .sidebar .fusion-tabs-widget .tab-holder,.sidebar .fusion-tabs-widget .tab-holder .news-list li{border-color:#ebeaea;}

.fusion-sharing-box{background-color:#f6f6f6;}

.grid-layout .post .post-wrapper,.timeline-layout .post,.blog-timeline-layout .post, .portfolio .portfolio-boxed .portfolio-item-wrapper{background-color:transparent;}

.grid-layout .post .flexslider,.grid-layout .post .post-wrapper,.grid-layout .post .content-sep,.products li,.product-details-container,.product-buttons,.product-buttons-container, .product .product-buttons,.blog-timeline-layout .post,.blog-timeline-layout .post .content-sep,
.blog-timeline-layout .post .flexslider,.timeline-layout .post,.timeline-layout .post .content-sep,
.portfolio .portfolio-boxed .portfolio-item-wrapper, .portfolio .portfolio-boxed .content-sep,
.timeline-layout .post .flexslider,.timeline-layout h3.timeline-title, .fusion-blog-timeline .timeline-title{border-color:#ebeaea;}
.timeline-layout  .timeline-circle,.timeline-layout .timeline-title,.blog-timeline-layout  .timeline-circle,.blog-timeline-layout .timeline-title{background-color:#ebeaea;}
.timeline-icon,.timeline-arrow:before,.blog-timeline-layout timeline-icon,.blog-timeline-layout .timeline-arrow:before{color:#ebeaea;}

	#bbpress-forums li.bbp-header,
	#bbpress-forums div.bbp-reply-header,#bbpress-forums #bbp-single-user-details #bbp-user-navigation li.current a,div.bbp-template-notice, div.indicator-hint{ background:#ebeaea; }
	#bbpress-forums .bbp-replies div.even { background: transparent; }

	#bbpress-forums ul.bbp-lead-topic, #bbpress-forums ul.bbp-topics, #bbpress-forums ul.bbp-forums, #bbpress-forums ul.bbp-replies, #bbpress-forums ul.bbp-search-results,
	#bbpress-forums li.bbp-body ul.forum, #bbpress-forums li.bbp-body ul.topic,
	#bbpress-forums div.bbp-reply-content,#bbpress-forums div.bbp-reply-header,
	#bbpress-forums div.bbp-reply-author .bbp-reply-post-date,
	#bbpress-forums div.bbp-topic-tags a,#bbpress-forums #bbp-single-user-details,div.bbp-template-notice, div.indicator-hint,
	.bbp-arrow{ border-color:#ebeaea; }


#posts-container.grid-layout {
	margin: 0 -20px;
}
#posts-container.grid-layout .post {
	padding: 20px;
}

.quicktags-toolbar input {
	background: linear-gradient(to top, #cabea8, #ffffff ) #3E3E3E;
	background: -o-linear-gradient(to top, #cabea8, #ffffff ) #3E3E3E;
	background: -moz-linear-gradient(to top, #cabea8, #ffffff ) #3E3E3E;
	background: -webkit-linear-gradient(to top, #cabea8, #ffffff ) #3E3E3E;
	background: -ms-linear-gradient(to top, #cabea8, #ffffff ) #3E3E3E;
	background: linear-gradient(to top, #cabea8, #ffffff ) #3E3E3E;

	background-image: -webkit-gradient(
		linear,
		left top,
		left bottom,
		color-stop(0, #ffffff),
		color-stop(1, #cabea8)
	);
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffff', endColorstr='#cabea8')
			progid: DXImageTransform.Microsoft.Alpha(Opacity=0);

	border: 1px solid #d2d2d2;
	color: #aaa9a9;
}

.quicktags-toolbar input:hover {
	background: #ffffff;
}


@media only screen and (max-width: 940px){
	.page-title .breadcrumbs{display:none;}
}
@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) and (orientation: portrait){
	.page-title .breadcrumbs{display:none;}
}




#nav.nav-holder .navigation > li > a{height:40px;line-height:40px;}

#nav ul li, #sticky-nav ul li { padding-right: 28px; }
.rtl #nav > .fusion-navbar-nav > li, .rtl #small-nav > .fusion-navbar-nav > li, .rtl #sticky-nav > .fusion-navbar-nav > li { padding-left: 28px; }

#nav ul li > a, #sticky-nav ul li > a, .navigation li.current-menu-ancestor > a,
#nav ul li > a, #sticky-nav ul li > a, .navigation li.current-menu-ancestor > a
{ border-top-width: 3px;}

.side-header-left #side-header #nav.nav-holder .navigation li.current-menu-ancestor > a,
.side-header-left #side-header #nav.nav-holder .navigation li.current-menu-item > a
{ border-right-width: 3px;}

.side-header-right #side-header #nav.nav-holder .navigation li.current-menu-ancestor > a,
.side-header-right #side-header #nav.nav-holder .navigation li.current-menu-item > a 
{ border-left-width: 3px;}

.ei-slider{width:100%;}

.ei-slider{height:400px;}

.button.default,.gform_wrapper .gform_button,#comment-submit,.woocommerce form.checkout #place_order,.woocommerce .single_add_to_cart_button, .woocommerce button.button,#reviews input#submit,.woocommerce .login .button,.woocommerce .register .button,
.bbp-submit-wrapper button,.wpcf7-form input[type="submit"], .wpcf7-submit, .bbp-submit-wrapper .button,#bbp_user_edit_submit, .ticket-selector-submit-btn[type=submit], .gform_page_footer input[type=button]
{border-color:#780404;}
.button.default:hover,.gform_wrapper .gform_button:hover,#comment-submit:hover,.woocommerce form.checkout #place_order:hover.woocommerce .single_add_to_cart_button:hover, .woocommerce button.button:hover,#reviews input#submit:hover,.woocommerce .login .button:hover,.woocommerce .register .button:hover,
.bbp-submit-wrapper button:hover,.wpcf7-form input[type="submit"]:hover, .wpcf7-submit:hover, .bbp-submit-wrapper .button:hover,#bbp_user_edit_submit:hover, .ticket-selector-submit-btn[type=submit]:hover, .gform_page_footer input[type=button];hover
{border-color:#780404;}

.button.default,.button-default,.gform_wrapper .gform_button,#comment-submit,.woocommerce .avada-shipping-calculator-form .button,.woocommerce form.checkout #place_order,.woocommerce .single_add_to_cart_button, .woocommerce button.button,#reviews input#submit,.woocommerce .login .button,.woocommerce .register .button,
.bbp-submit-wrapper button,.wpcf7-form input[type="submit"], .wpcf7-submit, .bbp-submit-wrapper .button,#bbp_user_edit_submit, .ticket-selector-submit-btn[type=submit], .gform_page_footer input[type=button]
{
	text-shadow:none;
	box-shadow: none;
}

.button.default.button-3d.button-small, .fusion-button.button-small.button-3d, .ticket-selector-submit-btn[type=submit]
{
	-webkit-box-shadow: inset 0px 1px 0px #fff, 0px 2px 0px #780404, 1px 4px 4px 2px rgba(0,0,0,0.3);
	-moz-box-shadow: 	inset 0px 1px 0px #fff, 0px 2px 0px #780404, 1px 4px 4px 2px rgba(0,0,0,0.3);
	box-shadow: 		inset 0px 1px 0px #fff, 0px 2px 0px #780404, 1px 4px 4px 2px rgba(0,0,0,0.3);		
}		
.button.default.button-3d.button-small:active, .fusion-button.button-small.button-3d:active
{
	-webkit-box-shadow: inset 0px 1px 0px #fff, 0px 1px 0px #780404, 1px 4px 4px 2px rgba(0,0,0,0.3);
	-moz-box-shadow: 	inset 0px 1px 0px #fff, 0px 1px 0px #780404, 1px 4px 4px 2px rgba(0,0,0,0.3);
	box-shadow: 		inset 0px 1px 0px #fff, 0px 1px 0px #780404, 1px 4px 4px 2px rgba(0,0,0,0.3);		
}
.button.default.button-3d.button-medium, .fusion-button.button-medium.button-3d
{
	-webkit-box-shadow: inset 0px 1px 0px #fff, 0px 3px 0px #780404, 1px 5px 5px 3px rgba(0,0,0,0.3);
	-moz-box-shadow: 	inset 0px 1px 0px #fff, 0px 3px 0px #780404, 1px 5px 5px 3px rgba(0,0,0,0.3);
	box-shadow: 		inset 0px 1px 0px #fff, 0px 3px 0px #780404, 1px 5px 5px 3px rgba(0,0,0,0.3);		
}
.button.default.button-3d.button-medium:active .fusion-button.button-medium.button-3d:active
{
	-webkit-box-shadow: inset 0px 1px 0px #fff, 0px 1px 0px #780404, 1px 5px 5px 3px rgba(0,0,0,0.3);
	-moz-box-shadow: 	inset 0px 1px 0px #fff, 0px 1px 0px #780404, 1px 5px 5px 3px rgba(0,0,0,0.3);
	box-shadow: 		inset 0px 1px 0px #fff, 0px 1px 0px #780404, 1px 5px 5px 3px rgba(0,0,0,0.3);		
}
.button.default.button-3d.button-large, .fusion-button.button-large.button-3d
{
	-webkit-box-shadow: inset 0px 1px 0px #fff, 0px 4px 0px #780404, 1px 6px 6px 3px rgba(0,0,0,0.3);
	-moz-box-shadow: 	inset 0px 1px 0px #fff, 0px 4px 0px #780404, 1px 6px 6px 3px rgba(0,0,0,0.3);
	box-shadow: 		inset 0px 1px 0px #fff, 0px 4px 0px #780404, 1px 6px 6px 3px rgba(0,0,0,0.3);		
}		
.button.default.button-3d.button-large:active, .fusion-button.button-large.button-3d:active
{
	-webkit-box-shadow: inset 0px 1px 0px #fff, 0px 1px 0px #780404, 1px 6px 6px 3px rgba(0,0,0,0.3);
	-moz-box-shadow: 	inset 0px 1px 0px #fff, 0px 1px 0px #780404, 1px 6px 6px 3px rgba(0,0,0,0.3);
	box-shadow: 		inset 0px 1px 0px #fff, 0px 1px 0px #780404, 1px 6px 6px 3px rgba(0,0,0,0.3);				
}
.button.default.button-3d.button-xlarge, .fusion-button.button-xlarge.button-3d
{
	-webkit-box-shadow: inset 0px 1px 0px #fff, 0px 5px 0px #780404, 1px 7px 7px 3px rgba(0,0,0,0.3);
	-moz-box-shadow: 	inset 0px 1px 0px #fff, 0px 5px 0px #780404, 1px 7px 7px 3px rgba(0,0,0,0.3);
	box-shadow: 		inset 0px 1px 0px #fff, 0px 5px 0px #780404, 1px 7px 7px 3px rgba(0,0,0,0.3);		
}		
.button.default.button-3d.button-xlarge:active, .fusion-button.button-xlarge.button-3d:active
{
	-webkit-box-shadow: inset 0px 1px 0px #fff, 0px 2px 0px #780404, 1px 7px 7px 3px rgba(0,0,0,0f.3);
	-moz-box-shadow: 	inset 0px 1px 0px #fff, 0px 2px 0px #780404, 1px 7px 7px 3px rgba(0,0,0,0.3);
	box-shadow: 		inset 0px 1px 0px #fff, 0px 2px 0px #780404, 1px 7px 7px 3px rgba(0,0,0,0.3);					
}

.button.default,.fusion-button,.button-default,.gform_wrapper .gform_button,#comment-submit,.woocommerce form.checkout #place_order,.woocommerce .single_add_to_cart_button, .woocommerce button.button,#reviews input#submit,.woocommerce .login .button,.woocommerce .register .button,
.bbp-submit-wrapper .button,.wpcf7-form input[type="submit"], .wpcf7-submit, #bbp_user_edit_submit, .gform_page_footer input[type=button]
{border-width:0px;border-style: solid;}
.button.default:hover, .fusion-button.button-default:hover, .ticket-selector-submit-btn[type=submit]{border-width:0px;border-style: solid;}


.button.default,.button-default,#comment-submit,.woocommerce form.checkout #place_order,.woocommerce .single_add_to_cart_button, .woocommerce button.button,#reviews input#submit,.woocommerce .avada-shipping-calculator-form .button,.woocommerce .login .button,.woocommerce .register .button,
.bbp-submit-wrapper .button,.wpcf7-form input[type="submit"], .wpcf7-submit, #bbp_user_edit_submit, .ticket-selector-submit-btn[type=submit], .gform_page_footer input[type=button], .gform_wrapper .gform_button
{border-radius: 2px;}


#slidingbar-area a{text-shadow:none;}

.image .image-extras .image-extras-content h3,
.image .image-extras .image-extras-content h4,
.image .image-extras .image-extras-content a,
.image .image-extras .image-extras-content h4,
.image .image-extras .image-extras-content .cats,
.image .image-extras .image-extras-content .cats a,
.image .image-extras .image-extras-content .price,
.image .image-extras .image-extras-content * {
	text-shadow:none;
}

.footer-area a,.copyright{text-shadow:none;}

.reading-box{background-color:#f6f6f6;}

.isotope .isotope-item {
  -webkit-transition-property: top, left, opacity;
	 -moz-transition-property: top, left, opacity;
	  -ms-transition-property: top, left, opacity;
	   -o-transition-property: top, left, opacity;
		  transition-property: top, left, opacity;
}


.header-v4 #small-nav,.header-v5 #small-nav{background-color:#ebe0d1;}


.header-v5 #header .logo{float:left;}

.rtl #header-sticky.sticky-header .logo{ float: left; }


.bbp_reply_admin_links .admin_links_sep, .bbp-admin-links .admin_links_sep{
	display: none;
}


.woocommerce form.checkout .col-2, .woocommerce form.checkout #order_review_heading, .woocommerce form.checkout #order_review {
	display: none;
}



@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) and (orientation: portrait) {
		#wrapper .page-title-container{
		height:87px !important;
	}
	}

#side-header .side-header-content, #side-header #nav.nav-holder .navigation > li > a, .header-wrapper .header-social .avada-row, .header-wrapper #header .avada-row, #header-sticky .avada-row, .header-wrapper .header-v4 #small-nav .avada-row, .header-wrapper .header-v5 #small-nav .avada-row { padding-left: 0px; }

#side-header .side-header-content, #side-header #nav.nav-holder .navigation > li > a, .header-wrapper .header-social .avada-row, .header-wrapper #header .avada-row, #header-sticky .avada-row, .header-wrapper .header-v4 #small-nav .avada-row, .header-wrapper .header-v5 #small-nav .avada-row { padding-right: 0px; }


#side-header{width:280px;}
body.side-header-left #wrapper{margin-left:280px;}
body.side-header-right #wrapper{margin-right:280px;}
body.side-header-left #side-header #nav > ul > li > ul, body.side-header-left #side-header #nav .login-box, body.side-header-left #side-header #nav .cart-contents, body.side-header-left #side-header #nav .main-nav-search-form{left:279px;}
body.rtl #boxed-wrapper{ position: relative; }
body.rtl.layout-boxed-mode.side-header-left #side-header{ position: absolute; left: 0; top: 0; margin-left:0px; }
body.rtl.side-header-left #side-header .side-header-wrapper{ position: fixed; width:280px;}



#wrapper .header-wrapper .fusion-megamenu-wrapper .fusion-megamenu {
  -webkit-box-shadow: 0 2px 2px #999;
  -moz-box-shadow: 0 2px 2px #999;
  box-shadow: 0 2px 2px #999;
}
#wrapper #side-header .fusion-megamenu-wrapper .fusion-megamenu {
	-webkit-box-shadow: 0px 10px 50px -2px rgba(0, 0, 0, 0.14);
	-moz-box-shadow: 0px 10px 50px -2px rgba(0, 0, 0, 0.14);
	box-shadow: 0px 10px 50px -2px rgba(0, 0, 0, 0.14);
}
#nav ul ul,
#sticky-nav ul ul {
	-moz-box-shadow: 1px 1px 30px rgba(0, 0, 0, 0.06);
	-webkit-box-shadow: 1px 1px 30px rgba(0, 0, 0, 0.06);
	box-shadow: 1px 1px 30px rgba(0, 0, 0, 0.06);
}





			html, body {
				background-color:#d7d6d6;
			}
	body{
				background-color:#d7d6d6;
		
				background-image:url(http://adlc.uad.ac.id/wp-content/uploads/2015/10/bg_new.png);
		background-repeat:repeat;
						}
			
	#wrapper{
		max-width:1084px;
		margin:0 auto;
	}
	.wrapper_blank { display: block; }
	
		
	@media (min-width: 1014px) {
		body #header-sticky.sticky-header {
			width:1084px;
			left: 0;
			right: 0;
			margin:0 auto;
		}	
	}
	
		
		@media only screen and (min-width: 801px) and (max-width: 1014px){
		#wrapper{
			width:auto;
		}
	}
	@media only screen and (min-device-width: 801px) and (max-device-width: 1014px){
		#wrapper{
			width:auto;
		}
	}
		
	
	
		
		html { background: none; }
			

		@media only screen and (min-device-width: 768px) and (max-device-width: 1366px) and (orientation: portrait){
		#nav > ul > li, #sticky-nav > ul > li { padding-right: 25px; }
	}
	@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) and (orientation: landscape){
		#nav > ul > li, #sticky-nav > ul > li { padding-right: 25px; }
	}
	
		.page-title-container{
		background-image:url(http://adlc.uad.ac.id/wp-content/themes/ADLC/images/page_title_bg.png);
	}
	
		.page-title-container{
		background-color:#ffffff;
	}
	
	#header, #side-header{
			}

	#header, #side-header {
			}
	
	#header, #side-header{	
			}
	

		
	
	
	.rev_slider_wrapper{
		position:relative
	}

		.rev_slider_wrapper .shadow-left{
		position:absolute;
		pointer-events:none;
		background-image:url(https://adlc.uad.ac.id/wp-content/themes/ADLC/images/shadow-top.png);
		background-repeat:no-repeat;
		background-position:top center;
		height:42px;
		width:100%;
		top:0;
		z-index:99;
	}

	.rev_slider_wrapper .shadow-left{top:-1px;}
	
	
	.rev_slider_wrapper .shadow-right{
		position:absolute;
		pointer-events:none;
		background-image:url(https://adlc.uad.ac.id/wp-content/themes/ADLC/images/shadow-bottom.png);
		background-repeat:no-repeat;
		background-position:bottom center;
		height:32px;
		width:100%;
		bottom:0;
		z-index:99;
	}

	.avada-skin-rev{
		border-top: 1px solid #d2d3d4;
		border-bottom: 1px solid #d2d3d4;
		-moz-box-sizing: content-box;
		box-sizing: content-box;
	}

	.tparrows{border-radius:0;}

	.rev_slider_wrapper .tp-leftarrow, .rev_slider_wrapper .tp-rightarrow{
		opacity:0.8;
		position: absolute;
		top: 50% !important;
		margin-top:-31px !important;	
		width: 63px !important;
		height: 63px !important;
		background:none;
		background-color: rgba(0, 0, 0, 0.5) ;	
		color:#fff;
	}

	.rev_slider_wrapper .tp-leftarrow:before{
		content:"\e61e";
		-webkit-font-smoothing: antialiased;
	}

	.rev_slider_wrapper .tp-rightarrow:before{
		content:"\e620";
		-webkit-font-smoothing: antialiased;
	}

	.rev_slider_wrapper .tp-leftarrow:before, .rev_slider_wrapper .tp-rightarrow:before{
		position: absolute;
		padding:0;
		width: 100%;
		line-height: 63px;
		text-align: center;
		font-size: 25px;
		font-family: IcoMoon;

	}

	.rev_slider_wrapper .tp-leftarrow:before{
		margin-left: -2px;
	}

	.rev_slider_wrapper .tp-rightarrow:before{
		margin-left: -1px;
	}

	.rev_slider_wrapper .tp-rightarrow{
		left:auto;
		right:0;
	}

	.no-rgba .rev_slider_wrapper .tp-leftarrow, .no-rgba .rev_slider_wrapper .tp-rightarrow{
		background-color:#ccc ;
	}

	.rev_slider_wrapper:hover .tp-leftarrow,.rev_slider_wrapper:hover .tp-rightarrow{
		display:block;
		opacity:0.8;
	}

	.rev_slider_wrapper .tp-leftarrow:hover, .rev_slider_wrapper .tp-rightarrow:hover{
		opacity:1;
	}

	.rev_slider_wrapper .tp-leftarrow{
		background-position: 19px 19px ;
		left: 0;
		margin-left:0;
		z-index:100;
	}

	.rev_slider_wrapper .tp-rightarrow{
		background-position: 29px 19px ;
		right: 0;
		margin-left:0;
		z-index:100;
	}

	.rev_slider_wrapper .tp-leftarrow.hidearrows,
	.rev_slider_wrapper .tp-rightarrow.hidearrows {
		opacity: 0;
	}

	.tp-bullets .bullet.last{
		clear:none;
	}
		

	
	#main{
		
				padding-top: 15px;
		
				padding-bottom: 10px;
			}

	
	#main{
				
		
		
	}

	
	.page-title-container{
		
		
		
			}
	
		.page-title-container{
		height:87px;
	}
	
	
		

	
		.width-100 .fullwidth-box, .width-100 .fusion-section-separator {
		margin-left: -15px; margin-right: -15px;
	}
	
		
	.woocommerce-invalid:after { content: 'Please enter correct details for this required field.'; display: inline-block; margin-top: 7px; color: red; }

		
		

	
		
	</style>

	
	
	
	
		
	
	</head>
<body class="home page-template-default page page-id-7 no-tablet-sticky-header no-mobile-sticky-header no-mobile-totop layout-boxed-mode mobile-menu-design-classic _masterslider _msp_version_3.0.6" data-spy="scroll">
			<div id="wrapper" class="">
					<div class="header-wrapper">
				<div class="header-v4">
		<div class="header-social">
		<div class="avada-row">
			<div class="alignleft">
							</div>
			<div class="alignright">
				<div class='top-menu'>
	<ul id="snav" class="menu">
		<li id="menu-item-121" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-121"><a href="https://adlc.uad.ac.id/contact/">Contact</a></li>
			</ul>
		<div class="mobile-topnav-holder"></div>
	</div>			</div>
		</div>
	</div>
		<header id="header">
		<div class="avada-row" style="padding-top:0px;padding-bottom:0px;">
			<div class="logo-container">
				<div class="logo" data-margin-right="0px" data-margin-left="0px" data-margin-top="0px" data-margin-bottom="0px" style="margin-right:0px;margin-top:0px;margin-left:0px;margin-bottom:0px;">
					<a href="https://adlc.uad.ac.id">
						<img src="http://adlc.uad.ac.id/wp-content/uploads/logo-adlc-new.png" alt="ADLC" class="normal_logo" />
											</a>
				</div>

								<form role="search" class="search searchform" method="get" action="https://adlc.uad.ac.id/">
					<div class="search-table">
						<div class="search-field">
							<input type="text" placeholder="Search..." value="" name="s" class="s" />
						</div>
						<div class="search-button">
							<input type="submit" class="searchsubmit" value="&#xf002;" />
						</div>
					</div>
				</form>
							</div>
			
					</div>
	</header>
	<div id="small-nav">
		<div class="avada-row">
						<nav id="nav" class="nav-holder">
							<ul class="navigation menu fusion-navbar-nav" style="overflow:hidden">
	<li  id="menu-item-40"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-7 current_page_item menu-item-40"  ><a    href="https://adlc.uad.ac.id/">HOME</a></li>
<li  id="menu-item-41"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-41 fusion-dropdown-menu"  ><a    href="https://adlc.uad.ac.id/profil/">PROFILE <span class="caret"></span></a>
<ul class="sub-menu">
	<li  id="menu-item-1181"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1181 fusion-dropdown-submenu"  ><a    href="https://adlc.uad.ac.id/vision-and-mission/">Vision and Mission</a></li>
	<li  id="menu-item-1182"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1182 fusion-dropdown-submenu"  ><a    href="https://adlc.uad.ac.id/struktur-organisasi/">Organization Structure</a></li>
</ul>
</li>
<li  id="menu-item-173"  class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-173 fusion-dropdown-menu"  ><a    >PROGRAM <span class="caret"></span></a>
<ul class="sub-menu">
	<li  id="menu-item-44"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-44 fusion-dropdown-submenu"  ><a    href="https://adlc.uad.ac.id/toefl/">TOEFL Preparation Class</a></li>
	<li  id="menu-item-42"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-42 fusion-dropdown-submenu"  ><a    href="https://adlc.uad.ac.id/ge/">General English</a></li>
	<li  id="menu-item-45"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-45 fusion-dropdown-submenu"  ><a    href="https://adlc.uad.ac.id/bipa/">BIPA</a></li>
	<li  id="menu-item-43"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-43 fusion-dropdown-submenu"  ><a    href="https://adlc.uad.ac.id/ielts/">IELTS Preparation Class</a></li>
	<li  id="menu-item-269"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-269 fusion-dropdown-submenu"  ><a    href="https://adlc.uad.ac.id/ielts-foundation-class/">IELTS Foundation Class</a></li>
	<li  id="menu-item-46"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-46 fusion-dropdown-submenu"  ><a    href="https://adlc.uad.ac.id/esp/">ESP</a></li>
	<li  id="menu-item-49"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-49 fusion-dropdown-submenu"  ><a    href="https://adlc.uad.ac.id/children-course/">Children Course</a></li>
	<li  id="menu-item-50"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-50 fusion-dropdown-submenu"  ><a    href="https://adlc.uad.ac.id/placement-test/">Placement Test</a></li>
</ul>
</li>
<li  id="menu-item-448"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-448"  ><a    href="https://adlc.uad.ac.id/adept/">ADEPT</a></li>
<li  id="menu-item-2228"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2228"  ><a    href="https://adlc.uad.ac.id/adept-online/">ADEPT Online</a></li>
<li  id="menu-item-447"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-447"  ><a    href="https://adlc.uad.ac.id/tesol-teachers-workshop/">TESOL &#038; Teachers Workshop</a></li>
<li  id="menu-item-449"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-449"  ><a    href="https://adlc.uad.ac.id/proofreading-and-translating/">Proofreading and Translating</a></li>
<li  id="menu-item-218"  class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-218"  ><a    href="https://adlc.uad.ac.id/category/adept-scores/">ADEPT Scores</a></li>
<li  id="menu-item-1776"  class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1776 fusion-dropdown-menu"  ><a    >Freshmen <span class="caret"></span></a>
<ul class="sub-menu">
	<li  id="menu-item-1778"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1778 fusion-dropdown-submenu"  ><a    href="https://adlc.uad.ac.id/batch-2018/">Batch 2018</a></li>
	<li  id="menu-item-1779"  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1779 fusion-dropdown-submenu"  ><a    href="https://adlc.uad.ac.id/batch-2019/">Batch 2019</a></li>
</ul>
</li>
		</ul>
			</nav>
						<div class="mobile-nav-holder main-menu"></div>
					</div>
	</div>
		</div>				<div class="init-sticky-header"></div>		
			</div>
			
	<div id="sliders-container">
		</div>
							<div id="main" class="clearfix " style="">
		<div class="avada-row" style="">		<div id="content" style="float:left;">
				<div id="post-7" class="post-7 page type-page status-publish hentry">
			<span class="entry-title" style="display: none;">Home</span><span class="vcard" style="display: none;"><span class="fn"><a href="https://adlc.uad.ac.id/author/adlcuad/" title="Posts by adlcuad" rel="author">adlcuad</a></span></span><span class="updated" style="display:none;">2019-02-21T13:39:12+07:00</span>																		<div class="post-content">
				<div class="fusion-fullwidth fullwidth-box" style="background-color:#ffffff;background-attachment:scroll;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;border-color:#ffffff;border-bottom-width: 0px;border-top-width: 0px;border-bottom-style: solid;border-top-style: solid;padding-bottom:20px;padding-left:0px;padding-right:0px;padding-top:20px;"><div class="avada-row">
		<!-- MasterSlider -->
		<div id="P_MS5ff2df9eebba0" class="master-slider-parent ms-caro3d-template ms-parent-id-8" style="max-width:1000px;"  >

			
			<!-- MasterSlider Main -->
			<div id="MS5ff2df9eebba0" class="master-slider ms-skin-black-1" >
				 				 

			<div  class="ms-slide" data-delay="3" data-fill-mode="fill"   >
					<img src="https://adlc.uad.ac.id/wp-content/plugins/masterslider/public/assets/css/blank.gif" alt="" title="DSC03518" data-src="https://adlc.uad.ac.id/wp-content/uploads/DSC03518-1000x450.jpg" />


				</div>
			<div  class="ms-slide" data-delay="3" data-fill-mode="fill"   >
					<img src="https://adlc.uad.ac.id/wp-content/plugins/masterslider/public/assets/css/blank.gif" alt="" title="DSC03434" data-src="https://adlc.uad.ac.id/wp-content/uploads/DSC03434-1.jpg" />


				</div>
			<div  class="ms-slide" data-delay="3" data-fill-mode="fit"   >
					<img src="https://adlc.uad.ac.id/wp-content/plugins/masterslider/public/assets/css/blank.gif" alt="" title="" data-src="https://adlc.uad.ac.id/wp-content/uploads/Workshop-Committee-1000x450.jpg" />


				</div>
			<div  class="ms-slide" data-delay="3" data-fill-mode="fit"   >
					<img src="https://adlc.uad.ac.id/wp-content/plugins/masterslider/public/assets/css/blank.gif" alt="" title="" data-src="https://adlc.uad.ac.id/wp-content/uploads/ADEPT-Test-1000x450.jpg" />


				</div>
			<div  class="ms-slide" data-delay="3" data-fill-mode="fill"   >
					<img src="https://adlc.uad.ac.id/wp-content/plugins/masterslider/public/assets/css/blank.gif" alt="" title="DSC_0193" data-src="https://adlc.uad.ac.id/wp-content/uploads/DSC_0193-1000x450.jpg" />


				</div>
			<div  class="ms-slide" data-delay="3" data-fill-mode="fit"   >
					<img src="https://adlc.uad.ac.id/wp-content/plugins/masterslider/public/assets/css/blank.gif" alt="" title="DSC_0185" data-src="https://adlc.uad.ac.id/wp-content/uploads/DSC_0185-1000x450.jpg" />


				</div>
			<div  class="ms-slide" data-delay="3" data-fill-mode="fill"   >
					<img src="https://adlc.uad.ac.id/wp-content/plugins/masterslider/public/assets/css/blank.gif" alt="" title="P1170114" data-src="https://adlc.uad.ac.id/wp-content/uploads/P1170114-1000x450.jpg" />


				</div>
			<div  class="ms-slide" data-delay="3" data-fill-mode="fit"   >
					<img src="https://adlc.uad.ac.id/wp-content/plugins/masterslider/public/assets/css/blank.gif" alt="" title="" data-src="https://adlc.uad.ac.id/wp-content/uploads/DSC03098-1000x450.jpg" />


				</div>
			<div  class="ms-slide" data-delay="3" data-fill-mode="fit"   >
					<img src="https://adlc.uad.ac.id/wp-content/plugins/masterslider/public/assets/css/blank.gif" alt="" title="" data-src="https://adlc.uad.ac.id/wp-content/uploads/DSC02843-1000x450.jpg" />


				</div>
			<div  class="ms-slide" data-delay="3" data-fill-mode="fill"   >
					<img src="https://adlc.uad.ac.id/wp-content/plugins/masterslider/public/assets/css/blank.gif" alt="" title="img_1265" data-src="https://adlc.uad.ac.id/wp-content/uploads/IMG_1265-1000x450.jpg" />


				</div>
			<div  class="ms-slide" data-delay="3" data-fill-mode="fit"   >
					<img src="https://adlc.uad.ac.id/wp-content/plugins/masterslider/public/assets/css/blank.gif" alt="" title="" data-src="https://adlc.uad.ac.id/wp-content/uploads/IMG_1231-1000x450.jpg" />


				</div>
			<div  class="ms-slide" data-delay="3" data-fill-mode="fit"   >
					<img src="https://adlc.uad.ac.id/wp-content/plugins/masterslider/public/assets/css/blank.gif" alt="" title="" data-src="https://adlc.uad.ac.id/wp-content/uploads/DSC02068-1000x450.jpg" />


				</div>
			<div  class="ms-slide" data-delay="3" data-fill-mode="fill"   >
					<img src="https://adlc.uad.ac.id/wp-content/plugins/masterslider/public/assets/css/blank.gif" alt="" title="DSC02461" data-src="https://adlc.uad.ac.id/wp-content/uploads/DSC02461-1000x450.jpg" />


				</div>
			<div  class="ms-slide" data-delay="3" data-fill-mode="fit"   >
					<img src="https://adlc.uad.ac.id/wp-content/plugins/masterslider/public/assets/css/blank.gif" alt="" title="" data-src="https://adlc.uad.ac.id/wp-content/uploads/DSC02456-1000x450.jpg" />


				</div>
			<div  class="ms-slide" data-delay="3" data-fill-mode="fit"   >
					<img src="https://adlc.uad.ac.id/wp-content/plugins/masterslider/public/assets/css/blank.gif" alt="" title="" data-src="https://adlc.uad.ac.id/wp-content/uploads/DSC02418-1000x450.jpg" />


				</div>
			<div  class="ms-slide" data-delay="3" data-fill-mode="fit"   >
					<img src="https://adlc.uad.ac.id/wp-content/plugins/masterslider/public/assets/css/blank.gif" alt="" title="" data-src="https://adlc.uad.ac.id/wp-content/uploads/BIPA-1000x450.jpg" />


				</div>

			</div>
			<!-- END MasterSlider Main -->

			 
		</div>
		<!-- END MasterSlider -->

		<script>
		( window.MSReady = window.MSReady || [] ).push( function( $ ) {

			"use strict";
			var masterslider_bba0 = new MasterSlider();

			// slider controls
			// slider setup
			masterslider_bba0.setup("MS5ff2df9eebba0", {
				width           : 1000,
				height          : 450,
				minHeight       : 0,
				space           : 0,
				start           : 1,
				grabCursor      : true,
				swipe           : true,
				mouse           : true,
				keyboard        : false,
				layout          : "boxed",
				wheel           : false,
				autoplay        : true,
				instantStartLayers:false,
				loop            : true,
				shuffle         : false,
				preload         : 0,
				heightLimit     : true,
				autoHeight      : false,
				smoothHeight    : true,
				endPause        : false,
				overPause       : true,
				fillMode        : "fill",
				centerControls  : true,
				startOnAppear   : false,
				layersMode      : "center",
				autofillTarget  : "",
				hideLayers      : false,
				fullscreenMargin: 0,
				speed           : 90,
				dir             : "h",
				parallaxMode    : 'swipe',
				view            : "fade"
			});

			
			window.masterslider_instances = window.masterslider_instances || [];
			window.masterslider_instances.push( masterslider_bba0 );
		});
		</script>

</div></div><div class="fusion-fullwidth fullwidth-box" style="background-color:#ffffff;background-attachment:scroll;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;border-color:#ffffff;border-bottom-width: 0px;border-top-width: 0px;border-bottom-style: solid;border-top-style: solid;padding-bottom:1px;padding-left:0px;padding-right:0px;padding-top:20px;"><div class="avada-row">
<p style="text-align: center;"><strong>Welcome to Ahmad Dahlan Language Center</strong></p>
<p>Ahmad Dahlan Language Center was established as one of the academic centers of Ahmad Dahlan University, Yogyakarta. Our mission is to help students of Ahmad Dahlan University and people at large develop literacy skills in English (and other languages) by providing language services ranging from skill-oriented courses to language skill assessment.  We also aspire to promote scholarly discussions and research on language teaching methodologies, language policies, curriculum designs and material development.</p>
<p>ADLC offers a whole range of programs to suit different purposes. Whether you are school-aged students wishing to excel in English subject, university students in need of upgrading TOEFL scores or improving proficiency or lecturers aspiring to undertake oversea studies and thereby required to pass IELTS, ADLC is committed to helping you achieve your goals.</p>
<p>For many academics,  international English tests such as IELTS and TOEFL can often pose an insurmountable obstacle to advance in their professional career. We try to recognize this and have worked hard to identify possible ways and strategies to help you meet the minimum requirement for both tests. Study with us and witness how you make progress as you are engaging in an interesting IELTS /TOEFL training at ADLC</p>
<p>ADLC also caters for non-Indonesian nationalities who wish to master our unique national language Bahasa Indonesia through our <strong>Bahasa Indonesia for Foreigners</strong> or <strong>BIPA (Bahasa Indonesia untuk Penutur Asing)</strong>. Indonesia has over 300 different local languages, each with different accents and varieties. However, they all speak Bahasa Indonesia as the national language. Mastering Indonesian language will guarantee you the privilege to be able to communicate with Indonesian people in all parts of Indonesia. Make sure you do not miss out this opportunity and get yourself immersed with local people.</p>
<p>At ADLC we truly value your precious time. We pay attention to learners’ unique individual context and differences and therefore our approach to teaching will ensure you learn with us at your comfortable pace whilst improving day-by-day towards your mastery of English or Bahasa Indonesia. As we are committed to delivering memorable learning experience to you, we train our teaching staff with the most cutting edge teaching methods to ensure our quality of delivery.</p>
<p>While our website is in its fledgling state,  we hope it will provide you with a flavor of what ADLC is like.</p>
<p>Thank you for your visit</p>
<p>Ahmad Budairi<br />
Head of Ahmad Dahlan Language Center</div></div><div class="fusion-fullwidth fullwidth-box" style="background-color:#cabea8;background-attachment:scroll;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;border-color:#ffffff;border-bottom-width: 0px;border-top-width: 0px;border-bottom-style: solid;border-top-style: solid;padding-bottom:1px;padding-left:0px;padding-right:0px;padding-top:20px;"><div class="avada-row"><div class="fusion-tabs fusion-tabs-1 classic nav-not-justified horizontal-tabs"><style type="text/css">.fusion-tabs.fusion-tabs-1 .nav-tabs li a{border-top-color:#ebeaea;background-color:#ebeaea;}.fusion-tabs.fusion-tabs-1 .nav-tabs{background-color:#ffffff;}.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a,.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a:hover,.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a:focus{border-right-color:#ffffff;}.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a,.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a:hover,.fusion-tabs.fusion-tabs-1 .nav-tabs li.active a:focus{background-color:#ffffff;}.fusion-tabs.fusion-tabs-1 .nav-tabs li a:hover{background-color:#ffffff;border-top-color:#ffffff;}.fusion-tabs.fusion-tabs-1 .tab-pane{background-color:#ffffff;}.fusion-tabs.fusion-tabs-1 .nav,.fusion-tabs.fusion-tabs-1 .nav-tabs,.fusion-tabs.fusion-tabs-1 .tab-content .tab-pane{border-color:#ebeaea;}</style><div class="nav"><ul class="nav-tabs"><li class="active"><a class="tab-link" id="news" href="#7faffd79b55bf9497" data-toggle="tab"><i class="fa fontawesome-icon fa-book"></i>News</a></li><li><a class="tab-link" id="upcomingevents" href="#678c931e2bb239a66" data-toggle="tab"><i class="fa fontawesome-icon fa-calendar"></i>Upcoming Events</a></li><li><a class="tab-link" id="adeptscores" href="#d6fc6966087b71313" data-toggle="tab"><i class="fa fontawesome-icon fa-book"></i>ADEPT Scores</a></li></ul></div><div class="tab-content"><div class="tab-pane fade in active" id="7faffd79b55bf9497"><div class="fusion-recent-posts avada-container layout-thumbnails-on-side layout-columns-1"><section class="fusion-columns columns fusion-columns-1 columns-1" style="width:100%;"><div class="row holder"><div class="fusion-column column col col-lg-12 col-md-12 col-sm-12"><div class="recent-posts-content"><span class="vcard" style="display: none;"><span class="fn"><a href="https://adlc.uad.ac.id/author/ita/" title="Posts by Admin Ita" rel="author">Admin Ita</a></span></span><span class="updated" style="display:none;">2020-11-20T15:33:30+07:00</span><h4 class="entry-title"><a href="https://adlc.uad.ac.id/adlc-toefl-training/">ADLC TOEFL TRAINING</a></h4><p class="meta"><span><span class="date">November 20th, 2020</span></span><span class="meta-separator">|</span><span><span>Comments Off<span class="screen-reader-text"> on ADLC TOEFL TRAINING</span></span></span></p></div></div><div class="fusion-clearfix"></div><div class="fusion-column column col col-lg-12 col-md-12 col-sm-12"><div class="recent-posts-content"><span class="vcard" style="display: none;"><span class="fn"><a href="https://adlc.uad.ac.id/author/ita/" title="Posts by Admin Ita" rel="author">Admin Ita</a></span></span><span class="updated" style="display:none;">2020-11-20T15:31:12+07:00</span><h4 class="entry-title"><a href="https://adlc.uad.ac.id/academic-support-session-for-uad-lecturers/">Academic Support Session for UAD Lecturers</a></h4><p class="meta"><span><span class="date">November 20th, 2020</span></span><span class="meta-separator">|</span><span><span>Comments Off<span class="screen-reader-text"> on Academic Support Session for UAD Lecturers</span></span></span></p></div></div><div class="fusion-clearfix"></div><div class="fusion-column column col col-lg-12 col-md-12 col-sm-12"><div class="fusion-flexslider flexslider floated-slideshow"><ul class="slides"><li><a href="https://adlc.uad.ac.id/adept-online-registration-schedule/"><img src="https://adlc.uad.ac.id/wp-content/uploads/WhatsApp-Image-2020-04-18-at-11.18.58-460x295.jpeg" alt="WhatsApp Image 2020-04-18 at 11.18.58"/></a></li></ul></div><div class="recent-posts-content"><span class="vcard" style="display: none;"><span class="fn"><a href="https://adlc.uad.ac.id/author/ita/" title="Posts by Admin Ita" rel="author">Admin Ita</a></span></span><span class="updated" style="display:none;">2020-04-21T11:03:02+07:00</span><h4 class="entry-title"><a href="https://adlc.uad.ac.id/adept-online-registration-schedule/">ADEPT Online Registration Schedule</a></h4><p class="meta"><span><span class="date">April 18th, 2020</span></span><span class="meta-separator">|</span><span><span>Comments Off<span class="screen-reader-text"> on ADEPT Online Registration Schedule</span></span></span></p></div></div><div class="fusion-clearfix"></div></div></section></div>
<p style="text-align: right;"><a href="http://adlc.uad.ac.id/category/news/">More&#8230;</a></p>
<p></div><div class="tab-pane fade" id="678c931e2bb239a66"><div class="fusion-recent-posts avada-container layout-thumbnails-on-side layout-columns-1"><section class="fusion-columns columns fusion-columns-1 columns-1" style="width:100%;"><div class="row holder"><div class="fusion-column column col col-lg-12 col-md-12 col-sm-12"><div class="fusion-flexslider flexslider floated-slideshow"><ul class="slides"><li><a href="https://adlc.uad.ac.id/adlc-research-workshop-series-linking-theoretical-framework-to-data-analysis/"><img src="https://adlc.uad.ac.id/wp-content/uploads/s200_ahmad.budairi.jpg" alt="s200_ahmad.budairi"/></a></li></ul></div><div class="recent-posts-content"><span class="vcard" style="display: none;"><span class="fn"><a href="https://adlc.uad.ac.id/author/desy/" title="Posts by Admin Desy" rel="author">Admin Desy</a></span></span><span class="updated" style="display:none;">2017-12-06T09:10:28+07:00</span><h4 class="entry-title"><a href="https://adlc.uad.ac.id/adlc-research-workshop-series-linking-theoretical-framework-to-data-analysis/">ADLC Research Workshop Series: &#8220;Linking Theoretical Framework to Data Analysis&#8221;</a></h4><p class="meta"><span><span class="date">December 6th, 2017</span></span><span class="meta-separator">|</span><span><span>Comments Off<span class="screen-reader-text"> on ADLC Research Workshop Series: &#8220;Linking Theoretical Framework to Data Analysis&#8221;</span></span></span></p><div class="excerpt-container strip-html"><p>ADLC Research Workshop Series</p>
<p>&#8220;Linking Theoretical Framework to Data Analysis&#8221;</p>
<p> 	Audience: [&#8230;]</p>
</div></div></div><div class="fusion-clearfix"></div><div class="fusion-column column col col-lg-12 col-md-12 col-sm-12"><div class="fusion-flexslider flexslider floated-slideshow"><ul class="slides"><li><a href="https://adlc.uad.ac.id/adlc-research-series-writing-a-literature-review-in-humanity-and-social-sciences/"><img src="https://adlc.uad.ac.id/wp-content/uploads/s200_ahmad.budairi.jpg" alt="s200_ahmad.budairi"/></a></li></ul></div><div class="recent-posts-content"><span class="vcard" style="display: none;"><span class="fn"><a href="https://adlc.uad.ac.id/author/desy/" title="Posts by Admin Desy" rel="author">Admin Desy</a></span></span><span class="updated" style="display:none;">2017-12-06T09:10:59+07:00</span><h4 class="entry-title"><a href="https://adlc.uad.ac.id/adlc-research-series-writing-a-literature-review-in-humanity-and-social-sciences/">ADLC Research Series: &#8220;Writing a Literature Review in Humanity and Social Sciences&#8221;</a></h4><p class="meta"><span><span class="date">December 6th, 2017</span></span><span class="meta-separator">|</span><span><span>Comments Off<span class="screen-reader-text"> on ADLC Research Series: &#8220;Writing a Literature Review in Humanity and Social Sciences&#8221;</span></span></span></p><div class="excerpt-container strip-html"><p>ADLC Research Series </p>
<p>&#8220;Writing a Literature Review in Humanity and [&#8230;]</p>
</div></div></div><div class="fusion-clearfix"></div><div class="fusion-column column col col-lg-12 col-md-12 col-sm-12"><div class="fusion-flexslider flexslider floated-slideshow"><ul class="slides"><li><a href="https://adlc.uad.ac.id/learning-strategy-training/"><img src="https://adlc.uad.ac.id/wp-content/uploads/s200_ahmad.budairi.jpg" alt="s200_ahmad.budairi"/></a></li></ul></div><div class="recent-posts-content"><span class="vcard" style="display: none;"><span class="fn"><a href="https://adlc.uad.ac.id/author/desy/" title="Posts by Admin Desy" rel="author">Admin Desy</a></span></span><span class="updated" style="display:none;">2017-12-06T09:17:13+07:00</span><h4 class="entry-title"><a href="https://adlc.uad.ac.id/learning-strategy-training/">Language Learning Strategy and Metacognition Training</a></h4><p class="meta"><span><span class="date">January 17th, 2017</span></span><span class="meta-separator">|</span><span><span>Comments Off<span class="screen-reader-text"> on Language Learning Strategy and Metacognition Training</span></span></span></p><div class="excerpt-container strip-html"><p>‘Learning How To Learn’ to improve proficiency in English. This [&#8230;]</p>
</div></div></div><div class="fusion-clearfix"></div></div></section></div>
<p style="text-align: right;"><a href="http://adlc.uad.ac.id/category/upcoming-events/">More&#8230;</a></p>
<p></div><div class="tab-pane fade" id="d6fc6966087b71313"><div class="fusion-recent-posts avada-container layout-date-on-side layout-columns-1"><section class="fusion-columns columns fusion-columns-1 columns-1" style="width:100%;"><div class="row holder"><div class="fusion-column column col col-lg-12 col-md-12 col-sm-12"><div class="date-and-formats"><div class="date-box"><span class="date">28</span><span class="month-year">12, 2020</span></div><div class="format-box"><i class="fusionicon-pen"></i></div></div><div class="recent-posts-content"><span class="vcard" style="display: none;"><span class="fn"><a href="https://adlc.uad.ac.id/author/ita/" title="Posts by Admin Ita" rel="author">Admin Ita</a></span></span><span class="updated" style="display:none;">2020-12-28T12:50:14+07:00</span><h4 class="entry-title"><a href="https://adlc.uad.ac.id/adept-scores-saturday-26-december-2020/">ADEPT Scores (Saturday, 26 December 2020)</a></h4><p class="meta"><span><span class="date">December 28th, 2020</span></span><span class="meta-separator">|</span><span><a href="https://adlc.uad.ac.id/adept-scores-saturday-26-december-2020/#respond">0 Comments</a></span></p><div class="excerpt-container strip-html"><p>Below is the result of ADEPT Test held on Saturday, 26 December 2020. Please refer to your name to check your score.  [&#8230;]</p>
</div></div></div><div class="fusion-clearfix"></div><div class="fusion-column column col col-lg-12 col-md-12 col-sm-12"><div class="date-and-formats"><div class="date-box"><span class="date">28</span><span class="month-year">12, 2020</span></div><div class="format-box"><i class="fusionicon-pen"></i></div></div><div class="recent-posts-content"><span class="vcard" style="display: none;"><span class="fn"><a href="https://adlc.uad.ac.id/author/ita/" title="Posts by Admin Ita" rel="author">Admin Ita</a></span></span><span class="updated" style="display:none;">2020-12-28T09:16:17+07:00</span><h4 class="entry-title"><a href="https://adlc.uad.ac.id/adept-scores-thursday-24-december-2020/">ADEPT Scores (Thursday, 24 December 2020)</a></h4><p class="meta"><span><span class="date">December 28th, 2020</span></span><span class="meta-separator">|</span><span><a href="https://adlc.uad.ac.id/adept-scores-thursday-24-december-2020/#respond">0 Comments</a></span></p><div class="excerpt-container strip-html"><p>Below is the result of ADEPT Test held on Thursday, 24 December 2020. Please refer to your name to check your score.  [&#8230;]</p>
</div></div></div><div class="fusion-clearfix"></div><div class="fusion-column column col col-lg-12 col-md-12 col-sm-12"><div class="date-and-formats"><div class="date-box"><span class="date">26</span><span class="month-year">12, 2020</span></div><div class="format-box"><i class="fusionicon-pen"></i></div></div><div class="recent-posts-content"><span class="vcard" style="display: none;"><span class="fn"><a href="https://adlc.uad.ac.id/author/ita/" title="Posts by Admin Ita" rel="author">Admin Ita</a></span></span><span class="updated" style="display:none;">2020-12-26T09:16:06+07:00</span><h4 class="entry-title"><a href="https://adlc.uad.ac.id/adept-scores-wednesday-23-december-2020/">ADEPT Scores (Wednesday, 23 December 2020)</a></h4><p class="meta"><span><span class="date">December 26th, 2020</span></span><span class="meta-separator">|</span><span><a href="https://adlc.uad.ac.id/adept-scores-wednesday-23-december-2020/#respond">0 Comments</a></span></p><div class="excerpt-container strip-html"><p>Below is the result of ADEPT Test held on Wednesday, 23 December 2020. Please refer to your name to check your score.  [&#8230;]</p>
</div></div></div><div class="fusion-clearfix"></div></div></section></div>
<p style="text-align: right;"><a href="http://adlc.uad.ac.id/category/adept-scores/">More&#8230;</a></p>
<p></div></div></div></div></div>
							</div>
																	</div>
			</div>
			<div id="sidebar" class="sidebar" style="float:right;">
		<div id="media_image-2" class="widget widget_media_image"><div class="heading"><h3>Office Hours</h3></div><img width="227" height="227" src="https://adlc.uad.ac.id/wp-content/uploads/Jam-Kantor-ADLC.jpg" class="image wp-image-1787  attachment-227x227 size-227x227" alt="" style="max-width: 100%; height: auto;" /></div>		<div id="recent-posts-3" class="widget widget_recent_entries">		<div class="heading"><h3>Recent Updates</h3></div>		<ul>
											<li>
					<a href="https://adlc.uad.ac.id/adept-scores-saturday-26-december-2020/">ADEPT Scores (Saturday, 26 December 2020)</a>
									</li>
											<li>
					<a href="https://adlc.uad.ac.id/adept-scores-thursday-24-december-2020/">ADEPT Scores (Thursday, 24 December 2020)</a>
									</li>
											<li>
					<a href="https://adlc.uad.ac.id/adept-scores-wednesday-23-december-2020/">ADEPT Scores (Wednesday, 23 December 2020)</a>
									</li>
											<li>
					<a href="https://adlc.uad.ac.id/adept-scores-friday-18-december-2020/">ADEPT Scores (Friday, 18 December 2020)</a>
									</li>
											<li>
					<a href="https://adlc.uad.ac.id/adept-scores-wednesday-16-december-2020/">ADEPT Scores (Wednesday, 16 December 2020)</a>
									</li>
											<li>
					<a href="https://adlc.uad.ac.id/adept-scores-friday-11-december-2020/">ADEPT Scores (Friday, 11 December 2020)</a>
									</li>
											<li>
					<a href="https://adlc.uad.ac.id/adept-scores-friday-4-december-2020/">ADEPT Scores (Friday, 4 December 2020)</a>
									</li>
					</ul>
		</div><div id="categories-2" class="widget widget_categories"><div class="heading"><h3>Categories</h3></div>		<ul>
				<li class="cat-item cat-item-11"><a href="https://adlc.uad.ac.id/category/adept-scores/">ADEPT Scores</a>
</li>
	<li class="cat-item cat-item-3"><a href="https://adlc.uad.ac.id/category/news/">News</a>
</li>
	<li class="cat-item cat-item-21"><a href="https://adlc.uad.ac.id/category/teachers-workshop/">Teachers Workshop</a>
</li>
	<li class="cat-item cat-item-9"><a href="https://adlc.uad.ac.id/category/upcoming-events/">Upcoming Events</a>
</li>
		</ul>
			</div><div id="social_links-widget-2" class="widget social_links"><div class="heading"><h3>ADLC Social Media</h3></div>		<div class="fusion-social-networks boxed-icons">
								<a class="fusion-social-network-icon fusion-tooltip fusion-facebook fusionicon-facebook" href="https://www.facebook.com/ahmaddahlanlanguagecenter/" data-placement="top" data-title="Facebook" data-toggle="tooltip" data-original-title="" title="Facebook"  target="_self" style="color:#fffafa;background-color:#b70101;border-color:#b70101;padding:10px;border-radius:10px;"></a>
										<a class="fusion-social-network-icon fusion-tooltip fusion-twitter fusionicon-twitter" href="https://twitter.com/adlcuad" data-placement="top" data-title="Twitter" data-toggle="tooltip" data-original-title="" title="Twitter"  target="_self" style="color:#fffafa;background-color:#b70101;border-color:#b70101;padding:10px;border-radius:10px;"></a>
										<a class="fusion-social-network-icon fusion-tooltip fusion-instagram fusionicon-instagram" href="https://www.instagram.com/adlcuad/" data-placement="top" data-title="Instagram" data-toggle="tooltip" data-original-title="" title="Instagram"  target="_self" style="color:#fffafa;background-color:#b70101;border-color:#b70101;padding:10px;border-radius:10px;"></a>
										</div>
		</div>	</div>
				</div>
	</div>
					<footer class="footer-area">
		<div class="avada-row">
			<div class="fusion-columns row fusion-columns-4 columns columns-4">
							
								<div class="fusion-column col col-lg-3 col-md-3 col-sm-3 ">
								</div>
								
								<div class="fusion-column col col-lg-3 col-md-3 col-sm-3">
				<div id="contact_info-widget-2" class="footer-widget-col contact_info"><h3>Contact Info</h3>		<div class="contact-info-container">
				<p class="address"> Jl. Kapas 9, Semaki, Umbulharjo, Yogyakarta 55166</p>
		
				<p class="phone">Phone:  (0274) 563515, 511830 Ext. 1632</p>
		
		
				<p class="fax">Fax: 0274-564604</p>
		
				<p class="email">Email: <a href="mailto:adlc@uad.ac.id">adlc@uad.ac.id</a></p>
		
				</div>
		<div style="clear:both;"></div></div>				</div>
								
								<div class="fusion-column col col-lg-3 col-md-3 col-sm-3">
				<div id="nav_menu-2" class="footer-widget-col widget_nav_menu"><h3>Information for</h3><div class="menu-information-for-container"><ul id="menu-information-for" class="menu"><li id="menu-item-151" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-151"><a href="http://alumni.uad.ac.id">Alumni</a></li>
<li id="menu-item-152" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-152"><a href="http://bimawa.uad.ac.id">Student Information</a></li>
<li id="menu-item-153" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-153"><a href="http://pmb.uad.ac.id">Admission UAD</a></li>
</ul></div><div style="clear:both;"></div></div>				</div>
								
								<div class="fusion-column col last col-lg-3 col-md-3 col-sm-3">
								</div>
				
				
								<div class="fusion-clearfix"></div>
			</div>
		</div>
	</footer>
			<footer id="footer">
		<div class="avada-row">
			<div class="copyright-area-content">
				<div class="copyright">
					<div>Copyright 2015 ADLC UAD</div>
				</div>
								
				<div class="fusion-social-links-footer">
									</div>
							</div>
		</div>
	</footer>
			</div><!-- wrapper -->
	
		

		
	<!-- W3TC-include-js-head -->

	<script type='text/javascript' src='https://adlc.uad.ac.id/wp-includes/js/comment-reply.min.js?ver=5.3.2'></script>
<script type='text/javascript' src='https://adlc.uad.ac.id/wp-content/themes/ADLC/js/modernizr-min.js?ver=3.7.3'></script>
<script type='text/javascript' src='https://adlc.uad.ac.id/wp-content/themes/ADLC/js/jquery.carouFredSel-6.2.1-min.js?ver=3.7.3'></script>
<script type='text/javascript' src='https://adlc.uad.ac.id/wp-content/themes/ADLC/js/jquery.cycle.js?ver=3.7.3'></script>
<script type='text/javascript' src='https://adlc.uad.ac.id/wp-content/themes/ADLC/js/jquery.prettyPhoto-min.js?ver=3.7.3'></script>
<script type='text/javascript' src='https://adlc.uad.ac.id/wp-content/themes/ADLC/js/jquery.flexslider-min.js?ver=3.7.3'></script>
<script type='text/javascript' src='https://adlc.uad.ac.id/wp-content/themes/ADLC/js/jquery.fitvids-min.js?ver=3.7.3'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var js_local_vars = {"protocol":"1","theme_url":"https:\/\/adlc.uad.ac.id\/wp-content\/themes\/ADLC","dropdown_goto":"Go to...","mobile_nav_cart":"Shopping Cart","page_smoothHeight":"false","flex_smoothHeight":"false","language_flag":"","infinite_blog_finished_msg":"<em>All posts displayed.<\/em>","infinite_finished_msg":"<em>All items displayed.<\/em>","infinite_blog_text":"<em>Loading the next set of posts...<\/em>","portfolio_loading_text":"<em>Loading Portfolio Items...<\/em>","faqs_loading_text":"<em>Loading FAQ Items...<\/em>","order_actions":"Details","avada_rev_styles":"1","avada_styles_dropdowns":"0","blog_grid_column_spacing":"40","blog_pagination_type":"Pagination","custom_icon_image_retina":"","disable_mobile_animate_css":"1","portfolio_pagination_type":"Pagination","header_position":"Top","header_sticky":"0","ipad_potrait":"0","is_responsive":"1","layout_mode":"boxed","lightbox_animation_speed":"Fast","lightbox_autoplay":"0","lightbox_desc":"1","lightbox_deeplinking":"1","lightbox_gallery":"1","lightbox_opacity":"0.8","lightbox_post_images":"1","lightbox_slideshow_speed":"5000","lightbox_social":"1","lightbox_title":"1","logo_alignment":"Left","megamenu_max_width":"1100px","pagination_video_slide":"0","retina_icon_height":"","retina_icon_width":"","submenu_slideout":"1","sidenav_behavior":"Hover","site_width":"1024px","slideshow_autoplay":"1","slideshow_speed":"7000","status_lightbox_mobile":"1","status_totop_mobile":"0","status_vimeo":"0","status_yt":"0","testimonials_speed":"4000","tfes_animation":"sides","tfes_autoplay":"1","tfes_interval":"3000","tfes_speed":"800","tfes_width":"150","woocommerce_shop_page_columns":"4","mobile_menu_design":"classic","isotope_type":"masonry","page_title_fading":"0"};
/* ]]> */
</script>
<script type='text/javascript' src='https://adlc.uad.ac.id/wp-content/themes/ADLC/js/main.js?ver=3.7.3'></script>
<script type='text/javascript' src='https://adlc.uad.ac.id/wp-includes/js/wp-embed.min.js?ver=5.3.2'></script>
<script type='text/javascript' src='https://adlc.uad.ac.id/wp-content/plugins/masterslider/public/assets/js/jquery.easing.min.js?ver=3.0.6'></script>
<script type='text/javascript' src='https://adlc.uad.ac.id/wp-content/plugins/masterslider/public/assets/js/masterslider.min.js?ver=3.0.6'></script>

	
	<!--[if lte IE 8]>
	<script type="text/javascript" src="https://adlc.uad.ac.id/wp-content/themes/ADLC/js/respond.js"></script>
	<![endif]-->
</body>
</html>
